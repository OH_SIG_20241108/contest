#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include "genki_led.h"

static void led_task(void) {

    while (1) {
        GenkiLedToggle();
        usleep(1000 * 1000);
    }
}

static void start(void) {
    osThreadAttr_t attr;

    attr.name = "led_gpio";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024;
    attr.priority = 25;

    if (osThreadNew((osThreadFunc_t) led_task, NULL, &attr) == NULL) {
        printf("Create LED GPIO task Failed!\r\n");
    }
}

APP_FEATURE_INIT(start);