#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include "genki_base.h"
#include "genki_env_base.h"
#include "genki_web.h"

static void init_my_service(void) {

    genki_web_register(newWebService());
}

static void start(void) {
    // genki基础服务，启动后才可进行串口Python应用烧录，网络Python应用烧录
    genki_services_start();

    // 初始化自己的网页应用页面（可选）
    init_my_service();
}

APP_FEATURE_INIT(start);