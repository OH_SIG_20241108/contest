
#ifndef GENKI_WEB_PLOTCLOCK_H
#define GENKI_WEB_PLOTCLOCK_H


#include "pca9685.h"
#include "genki_web.h"

#define ANGLE_NOT_INIT -1000

static const int LEFT_START_ANGLE = 30;
static const int RIGHT_START_ANGLE = -30;


static const int SERVO_LEFT_PORT = 3;
static const int SERVO_RIGHT_PORT = 4;
static const int SERVO_BOTTOM_PORT = 15;
static int pre_angle_arr[16] = {
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
    ANGLE_NOT_INIT,
};

static void platform_up();

static void platform_down();

static void platform_shake();

static void platform_init();

static int update_serve_angle(double alpha, double beta);

static int servo_set_angle(int port, int angle);

static int servo_get_angle(int port);

genki_web_service_t* newPlotService(void);

#endif //GENKI_WEB_PLOTCLOCK_H
