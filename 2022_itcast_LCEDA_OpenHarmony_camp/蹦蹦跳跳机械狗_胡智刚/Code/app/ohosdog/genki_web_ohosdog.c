
#include "genki_web_ohosdog.h"

#include <stdio.h>
#include "utils_file.h"
#include <stdint.h>
#include <stdlib.h>
#include "dog.h"

static void save_html(const char *page, const char *html) {
    int fd = UtilsFileOpen(page, O_CREAT_FS | O_TRUNC_FS | O_WRONLY_FS, 0);

    if (fd > 0) {
        UtilsFileWrite(fd, html, strlen(html));
        UtilsFileClose(fd);
    }
}

static int doHtml(genki_web_request_t *request, genki_web_response_t *response) {
    if (request->method != GET) {
        return -1;
    }
    const char *page = "plot.html";
    unsigned int size;
    char buf[128];
    if (UtilsFileStat(page, &size) >= 0) {
        sprintf(buf, "%d", size);
        response->setHeader(response, "Content-Length", buf);

        int fd = UtilsFileOpen(page, O_RDONLY_FS, 0);
        if (fd > 0) {
            unsigned char num;
            while ((num = UtilsFileRead(fd, buf, 128)) > 0) {
                response->write(response, buf, num);
            }
            UtilsFileClose(fd);
        } else {
            sprintf(buf, "<html><body><h2 id='a'>ERROR</h2></body></html>");
            response->write(response, buf, strlen(buf));
        }
    } else {
        const char *html = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>传智教育元气派</title><style> body {user-select: none;text-align: center;}  table {margin: 0 auto}  td div {width: 150px;height: 150px;border: 1px solid darkgrey;display: flex;align-items: center;justify-content: center;color: black;}  #s {background-color: red;color: white;}  td div:active {background: deepskyblue;color: white;}  button {margin: 0.5rem;width: 9rem;height: 3rem;font-size: 1.2rem;border-radius: 1rem;}  button:hover {border-radius: 1rem;background-color: deepskyblue;color: aliceblue;}</style></head><body><h1>鸿蒙狗控制</h1><button id=\"a\">安装角度</button><br><br>端口:<input id=\"k\" value=\"0\">角度:<input id=\"l\" value=\"0\"><button id=\"b\">测试舵机端口</button><script> function u(p) {return `${p}`;}function g(i) {return document.getElementById(i);}function fe(e, f) {e.addEventListener('click', function (e) {/* do something*/f();});}function fg(u, f) {let x = new XMLHttpRequest();x.onreadystatechange = function () {if (this.readyState == 4 && this.status == 200) {f(x);}};x.open(\"GET\", u, true);x.send();}fe(g('a'), () => {fg(u('/dog/init'), () => {});});fe(g('b'), () => {let p = g('k').value;let a = g('l').value;fg(u(`/dog/test?p=${p}&a=${a}`), () => {});});</script></body></html>";
        size_t len = strlen(html);

        char buf[128];
        sprintf(buf, "%d", len);
        response->setHeader(response, "Content-Type", "text/html; charset=UTF-8");
        response->setHeader(response, "Content-Length", buf);
        response->write(response, html, len);
        save_html(page, html);
    }

    return 1;
}

static int doInit(genki_web_request_t *request, genki_web_response_t *response) {
    dog_config_install();

    return 1;
}

static int doTest(genki_web_request_t *request, genki_web_response_t *response) {
    char *p1 = strtok(request->queryString, "&");
    if (p1) {
        char *p2 = strtok(NULL, "");

        char *key1 = strtok(p1, "=");
        char *value1 = strtok(NULL, "");

        char *key2 = strtok(p2, "=");
        char *value2 = strtok(NULL, "");

        char str_port[64] = {0};
        char str_angle[64] = {0};
        unsigned char flag = 1;
        if (strcasecmp(key1, "p") == 0 && strcasecmp(key2, "a") == 0) {
            memcpy(str_port, value1, strlen(value1));
            memcpy(str_angle, value2, strlen(value2));
        } else if (strcasecmp(key2, "p") == 0 && strcasecmp(key1, "a") == 0) {
            memcpy(str_port, value2, strlen(value2));
            memcpy(str_angle, value1, strlen(value1));
        } else {
            flag = 0;
        }

        if (flag) {
            int port;
            int angle;
            sscanf(str_port, "%d", &port);
            sscanf(str_angle, "%d", &angle);

            dog_step();
        }
    }
    return 1;
}


genki_web_service_t* newDogService(void) {
    genki_web_service_t *service = genki_web_newService("DOG");
    service->addFilter(service, "/dog", doHtml);
    service->addFilter(service, "/dog/init", doInit);
    service->addFilter(service, "/dog/test", doTest);

    service->link_name = "Dog控制";
    service->link_url = "/dog";
    return service;
}