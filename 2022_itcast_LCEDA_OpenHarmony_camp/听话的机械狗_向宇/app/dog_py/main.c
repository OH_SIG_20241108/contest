#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>
#include "ohos_init.h"
#include "cmsis_os2.h"

#include "genki_base.h"
#include "genki_web_ohosdog.h"
#include "genki_web.h"
#include "genki_pin.h"
#include "dog.h"
#include "hi_uart.h"
#include "iot_uart.h"
#include "iot_gpio.h"
#include "iot_io.h"



#define WIFI_IOT_SUCCESS 0
#define UART_BUFF_SIZE 100
#define UART_TASK_STACK_SIZE 1024 * 1
#define UART_TASK_PRIO 25
uint8_t Bi_Zhang_Flag=0;
uint8_t uart_buff[UART_BUFF_SIZE] = {0};
uint8_t *uart_buff_ptr = uart_buff;
enum{
    Get_Down,//趴下
	Hand_shake,//握手
    Go_Forward,//前进
    Go_Backward,//后退
    Go_Left,//左转
    Go_Right,//右转
    Twist_Body,//扭身子
    Stretch_Oneself,//伸懒腰
    Obstacle_Avoidance,//避障模式
    Stand//站立
};

//检测串口指令
void SIZU_Uart_Cmd(char *str)      
{
    char  *Str;
    unsigned char ID=255;
	
    Str=&str[1];//定位到指令的数字部分“U1”
    ID=atoi(Str);
	
	if(strstr((const char *)str,"G")!=NULL)			//如果字符串str中包含有“G”
	{
		switch(ID)
		{
			case Get_Down:	//趴下  G0
                sithome();
                Bi_Zhang_Flag=0;
                printf("Get_Down\r\n");
				break;
			case Hand_shake:	// 握手G1
                wink(5);
                Bi_Zhang_Flag=0;
                printf("Handshake\r\n");
				break;
			case Go_Forward:	// 前进G2
                forward(5);
                Bi_Zhang_Flag=0;
                printf("Go_Forward\r\n");
				break;
			case Go_Backward:	// 后退G3
                 backward(5);
                 Bi_Zhang_Flag=0;
                 printf("Go_Backward\r\n");
				break;
            case Go_Left:	// 左转G4
                leftturn(5);
                Bi_Zhang_Flag=0;
                 printf("Go_Left\r\n");
				break;
             case Go_Right:	// 右转G5
                 rightturn(5);
                 Bi_Zhang_Flag=0;
                 printf("Go_Right\r\n");
				break;
            case Twist_Body:	// 扭身子
                 twist();
                 Bi_Zhang_Flag=0;
                 printf("LED_Add\r\n");
				break;
            case Stretch_Oneself:	//伸懒腰
                 printf("LED_Reduce\r\n");
                 Bi_Zhang_Flag=0;
                 stand3();
				break;
            case Obstacle_Avoidance:	//避障
                 printf("避障模式\r\n");
                Bi_Zhang_Flag=1;
				break;
			default://站立
				printf("%s ERROR",str);
                Bi_Zhang_Flag=0;
                standhome();
				break;
		}
	}
    memset(uart_buff,0,sizeof(uart_buff));
}


// static const char *data = "Hello, GenkiPi!\r\n";

static void init_service(void) {
    genki_web_register(newDogService());
}

static void UART_Task(void)
{
    IotUartAttribute uart_attr = {
        //baud_rate: 9600
        .baudRate = 9600,

        //data_bits: 8bits
        .dataBits = 8,
        .stopBits = 1,
        .parity = 0,
        .rxBlock = 0,
        .txBlock = 0,
    };
      //Initialize uart driver
    IoTUartInit(HI_UART_IDX_1, &uart_attr);
    while (1)
    {
        // printf("=======================================\r\n");
        // printf("*************SIZU_example**************\r\n");
        // printf("=======================================\r\n");

        // //通过串口1发送数据
        // IoTUartWrite(HI_UART_IDX_1, (unsigned char *)data, strlen(data));
        //超声波获取距离
        // AD = GetDistance();
        //通过串口1接收数据
        IoTUartRead(HI_UART_IDX_1, uart_buff_ptr, UART_BUFF_SIZE);
        SIZU_Uart_Cmd((char *)uart_buff_ptr);
        if(Bi_Zhang_Flag==1)
        {
            Bi_Zhang();
        }
        // printf("Uart1 read data:%s\r\n", uart_buff_ptr);
        // printf("Uart1 read data:%f\r\n", AD);
        usleep(100*1000);
    }

}

static void start(void) {
    osThreadAttr_t attr;
    IoTIoSetFunc(IOT_IO_NAME_5, IOT_IO_FUNC_5_UART1_RXD);
    IoTIoSetFunc(IOT_IO_NAME_6, IOT_IO_FUNC_6_UART1_TXD);
    IoTIoSetFunc(IOT_IO_NAME_10, IOT_IO_FUNC_10_I2C0_SDA);
    IoTIoSetFunc(IOT_IO_NAME_9, IOT_IO_FUNC_9_I2C0_SCL);
    IoTI2cInit(0, 400000);
    dog_init();
    genki_services_start();
    init_service();
   
    attr.name = "UART_Task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = UART_TASK_STACK_SIZE;
    attr.priority = UART_TASK_PRIO;

    if (osThreadNew((osThreadFunc_t)UART_Task, NULL, &attr) == NULL)
    {
        printf("[ADCExample] Falied to create UART_Task!\n");
    }
}

APP_FEATURE_INIT(start);