# 2022-九联科技&惠州学院&OpenHarmony共建样例活动作品提交说明

欢迎各位同学参与OpenHarmony 共建样例活动，提交你们样例的相关代码、说明文档等内容。

## 提交步骤

### 1. 提交环境准备

1、创建gitee账号，并绑定相关邮箱

2、签署dco协议，具体地址链接：https://dco.openharmony.cn/sign-dco

3、fork 官方仓库，地址: https://gitee.com/openharmony-sig/contest


### 2. 提交样例说明

大家在contest仓库/2022_Unionman_HZU_OpenHarmony_Demos/对应的队伍文件夹目录下，

参照作品提交要求，准备好相应的文档。然后发起提交即可。

##### 样例提交仓库地址

[https://gitee.com/openharmony-sig/contest/tree/master/2022_Unionman_HZU_OpenHarmony_Demos](https://gitee.com/openharmony-sig/contest/tree/master/2022_Unionman_HZU_OpenHarmony_Demos)

##### 样例提交要求

- 作品源码
- 作品说明文档
- 演示视频
- 每名成员的学习心得

**注意事项**

1. 建议将演示视频以链接形式（推荐bilibili）添加在说明文档内;

2. 说明文档要求用[md格式](https://gitee.com/openharmony-sig/contest/blob/master/docs/%E6%80%8E%E4%B9%88%E7%BC%96%E8%BE%91Markdown%E6%96%87%E6%A1%A3/README.md)编辑

3. 学习心得可参考[模板](https://gitee.com/openharmony-sig/contest/blob/master/2022_Unionman_HZU_OpenHarmony_Demos/01_knowledge_team/kenio_zhang_7_7.md)
   
4. 作品源码按照以下规范要求来提交;

   [OpenHarmony代码规范说明](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/contribute/%E8%B4%A1%E7%8C%AE%E4%BB%A3%E7%A0%81.md)


##### 样例评审规则

将从以下四个维度去评分
1. 项目技术上的难易度
2. 样例是否具备创新性、实用性及产品落地应用可行性
3. 是否体现了的OpenHarmony相关特性
4. 是否提供足够详细的说明文档指导其他开发者运行/使用这个项目？

### 3. 提交PR

完整的PR提交流程请参考如下文档：

https://gitee.com/openharmony-sig/contest/blob/master/2022-hisilicon-national-embedded-competition/README_zh.md