package com.itrui.buyitbackend.pojo;

import lombok.Data;

@Data
public class MessageSend {
    private int type;
    private Integer sender;
    private Integer receiver;
    private String content;
    public MessageSend(int send_type, Integer sender, Integer receiver, String content){
        this.type = send_type;
        this.sender = sender;
        this.receiver = receiver;
        this.content = content;
    }
    public MessageSend(int error_type){
        this.type = error_type;
    }
}
