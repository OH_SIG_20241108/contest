package com.itrui.buyitbackend.service;

import com.itrui.buyitbackend.pojo.MyAddress;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface MyAddressService {
    /**
     * 获取用户地址
     * @param account
     * @return
     */
    public List<MyAddress> getAllAddressByAccount(Integer account);

    /**
     * 删除地址
     * @param id
     * @return
     */
    public boolean delAddressById(Integer id);

    /**
     * 修改地址
     * @param myAddress
     * @return
     */
    public boolean updateAddress(MyAddress myAddress);

    /**
     * 添加地址信息
     * @param myAddress
     * @return
     */
    public boolean addAddress(MyAddress myAddress);
}
