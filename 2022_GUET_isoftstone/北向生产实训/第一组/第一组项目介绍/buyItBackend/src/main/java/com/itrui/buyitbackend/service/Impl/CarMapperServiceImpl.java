package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.CarMapper;
import com.itrui.buyitbackend.pojo.Car;
import com.itrui.buyitbackend.service.CarMapperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarMapperServiceImpl implements CarMapperService {

    @Autowired
    private CarMapper carMapper;

    @Override
    public List<Car> getAllCar(Integer account) {
        return carMapper.getAllCar(account);
    }

    @Override
    public boolean delCar(Integer id) {
        return carMapper.delCar(id) > 0;
    }

    @Override
    public boolean addCar(Car car) {
        return carMapper.addCar(car) > 0;
    }

    @Override
    public int getCarNum(Integer account) {
        return carMapper.getCarNum(account);
    }
}
