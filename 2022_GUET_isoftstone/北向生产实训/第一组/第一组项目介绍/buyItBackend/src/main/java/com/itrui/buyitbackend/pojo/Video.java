package com.itrui.buyitbackend.pojo;

import lombok.Data;

@Data
public class Video {

    private Integer videoId;
    private Integer likeNum;
    private Integer saveNum;
    private String commentNum;
    private Integer shareNum;
    private String likeSrc;
    private String saveSrc;
    private String name;
    private String title;
    private String video;

}
