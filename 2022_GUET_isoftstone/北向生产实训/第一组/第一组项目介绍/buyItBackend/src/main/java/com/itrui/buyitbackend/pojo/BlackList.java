package com.itrui.buyitbackend.pojo;

import lombok.Data;

@Data
public class BlackList {

    private Integer blacklistId ;
    private Integer blacklistUserid;
    private Integer blacklistTarget;
    private Integer blacklistShieldType;


}
