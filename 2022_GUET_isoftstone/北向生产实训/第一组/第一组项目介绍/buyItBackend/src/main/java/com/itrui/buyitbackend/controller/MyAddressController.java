package com.itrui.buyitbackend.controller;

import com.itrui.buyitbackend.common.Code;
import com.itrui.buyitbackend.common.Result;
import com.itrui.buyitbackend.pojo.MyAddress;
import com.itrui.buyitbackend.service.MyAddressService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/address")
public class MyAddressController {
    @Autowired
    private MyAddressService myAddressService;
    /**
     * 获取用户地址
     * @param account
     * @return
     */
    @GetMapping("/{account}")
    public Result getAllAddressByAccount(@PathVariable Integer account){

        List<MyAddress> allAddressByAccount = myAddressService.getAllAddressByAccount(account);
        return new Result(Code.GET_OK, allAddressByAccount,"查询成功");

    };

    /**
     * 删除地址
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result delAddressById(@PathVariable Integer id){
        boolean b = myAddressService.delAddressById(id);
        return new Result(b?Code.DELETE_OK:Code.DELETE_ERR,b?"删除成功":"删除失败");
    };

    /**
     * 修改地址
     * @param myAddress
     * @return
     */
    @PutMapping
    public Result updateAddress(@RequestBody MyAddress myAddress){
        log.info("修改的地址信息："+myAddress);
        boolean flag = myAddressService.updateAddress(myAddress);
        return new Result(flag?Code.UPDATE_OK:Code.UPDATE_ERR,flag?"修改成功":"修改失败");
    };

    /**
     * 添加地址信息
     * @param myAddress
     * @return
     */
    @PostMapping
    public Result addAddress(@RequestBody MyAddress myAddress){
        log.info("添加的地址信息："+myAddress);
        boolean flag = myAddressService.addAddress(myAddress);
        return new Result(flag?Code.SAVE_OK:Code.SAVE_ERR,flag?"添加成功":"添加失败");
    };

}
