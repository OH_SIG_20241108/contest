//export default {
//    props: {
//        isValid:{
//            default: 'true'
//        },
//    },
//    data() {
//        return {
//            inputText: [],
//            bit: false,
//            isValid: this.isValid
//        }
//    },
//
//    click(e) { //---一系列的键盘输入合法处理逻辑
//        let id = e.target.id
//        if (this.isValid === 'false') {
//            this.randomInput(id).then(texts => {
//                this.$emit('textType', {
//                    text: texts
//                });
//            })
//        }
//        else if (this.isValid === 'true') {
//            this.validInput(id).then(texts => {
//                this.$emit('textType', {
//                    text: texts
//                });
//            })
//        }
//        else {
//            console.info(`attribute "isValid" error , "isValid" only can be false | true`)
//        }
//    },
//
//    randomInput(id) {
//        return new Promise((res) => {
//            if (id !== 'x') {
//                this.inputText += `${id}`
//                res(this.inputText)
//            } else {
//                if (this.inputText.length !== 0) {
//                    this.inputText = this.inputText.slice(0, this.inputText.length - 1)
//                }
//                res(this.inputText)
//            }
//        })
//    },
//    validInput(id) {
//        return new Promise((res) => {
//            let len = this.inputText.length
//            if (id === '.') {
//                let a = this.inputText.indexOf('.')
//                if (a !== -1)this.bit = true
//                else this.bit = false
//                if (this.bit || (len === 2 && this.inputText[0] === '0' && this.inputText[1] === '.'))res(this.inputText)
//            }
//            if (len === 1) {
//                if (this.inputText[0] === '0' && id !== '.' && id !== 'x')res(this.inputText)
//            }
//            if (id === 'x') {
//                console.info('x')
//                if (len !== 0) {
//                    this.inputText = this.inputText.slice(0, len - 1)
//                    res(this.inputText)
//                }
//            }
//            if (len <= 7) {
//                if (id !== 'x') {
//                    this.inputText += `${id}`
//                    res(this.inputText)
//                }
//            }
//            res(this.inputText)
//        })
//    }
//}
export default {
    props: {
        isValid:{
            default:'false'
        },
        text:{
            default:''
        }
    },
    data() {
        return {
            inputText: [],
            bit: false,
            isValid: this.isValid,
            text:this.text
        }
    },
    onInit() {
        this.$watch('text', 'onPropertyChange');
    },
    onPropertyChange(newV, oldV) {
        //        console.info('text 属性变化 ' + newV + ' ' + oldV);
        this.inputText = this.text
    },

    click(e) {
        let id = e.target.id
        if (this.isValid === 'false') {
            this.randomInput(id).then(texts => {
                this.$emit('textType', {
                    text: texts
                });
            })
        }
        else if (this.isValid === 'true') {
            this.validInput(id).then(texts => {
                this.$emit('textType', {
                    text: texts
                });
            })
        }
        else {
            console.info(`attribute "isValid" error , "isValid" only can be false | true`)
        }
    },

    randomInput(id) {
        return new Promise((res) => {
            if (id !== 'x') {
                this.inputText += `${id}`
                res(this.inputText)
            } else {
                if (this.inputText.length !== 0) {
                    this.inputText = this.inputText.slice(0, this.inputText.length - 1)
                }
                res(this.inputText)
            }
        })
    },
    validInput(id) {
        return new Promise((res) => {
            let len = this.inputText.length
            if (id === '.') {
                let a = this.inputText.indexOf('.')
                if (a !== -1)this.bit = true
                else this.bit = false
                if (this.bit || (len === 2 && this.inputText[0] === '0' && this.inputText[1] === '.'))res(this.inputText)
            }
            if (len === 1) {
                if (this.inputText[0] === '0' && id !== '.' && id !== 'x')res(this.inputText)
            }
            if (id === 'x') {
                console.info('x')
                if (len !== 0) {
                    this.inputText = this.inputText.slice(0, len - 1)
                    res(this.inputText)
                }
            }
            if (len <= 7) {
                if (id !== 'x') {
                    this.inputText += `${id}`
                    res(this.inputText)
                }
            }
            res(this.inputText)
        })
    }
}
