import { initializeOnStartup } from '../model/SportsDataModels'
import { Category, SportsData, RecordData } from '../model/SportsData'
import router from '@ohos.router';
import prompt from '@ohos.prompt'


export var RecordDataArray: Array<RecordData> = []
export var RecordSports: Array<SportsData> = []

@Entry
@Component
struct SportsCategoryList {
  @State RecordDataArray: Array<RecordData> = RecordDataArray
  @State RecordSports: Array<SportsData> = RecordSports
  @State currentIndex: number = 0;
  @State search_item: string = '运动名称';
  private sportsItem: SportsData[] = initializeOnStartup()

  @Builder bottomBarBuilder(name: string, image_active: Resource, image_inactive: Resource, index: number) {
    Column() {
      Image(this.currentIndex === index ? image_active : image_inactive).width(32).aspectRatio(1).fillColor(this.currentIndex === index ? '#3ECF69' : '#bfbfbf')
      Text(name).fontColor(this.currentIndex === index ? '#3ECF69' : '#bfbfbf').fontSize(18).margin({ bottom: 3 })
    }.alignItems(HorizontalAlign.Center).width('100%')
  }

  build() {
    Tabs({ barPosition: BarPosition.End }) {
      TabContent() {
        Column() {
          Row() {
            Image($r('app.media.search'))
              .width(26)
              .height(26)
              .objectFit(ImageFit.Cover)
              .margin({ left: 10 })

            TextInput({ placeholder: '请输入运动名称' })
              .width('60%')
              .height(30)
              .backgroundColor(Color.White)
              .onChange((value: string) => {
                this.search_item = value
              })

            Blank()

            Button('搜索')
              .backgroundColor('#3ECF69')
              .borderRadius(30)
              .fontSize(15)
              .fontColor(Color.White)
              .height(30)
              .width('20%')
              .margin({ left: 8, right: 3, top: 3, bottom: 3 })
              .onClick(() => {
                router.push({
                  url: 'pages/search_result',
                  params: {
                    sports: this.search_item
                  }
                })
              })
          } //search
          .borderColor('#3ECF69')
          .borderWidth(2)
          .borderRadius(50)
          .backgroundColor(Color.White)
          .width('90%')
          .height(40)
          .margin({ top: 5, bottom: 5, left: 5, right: 5 })

          SportsCategory({ sportsItems: this.sportsItem })
        }
        .backgroundImage($r('app.media.background'))
        .backgroundImageSize({ width: '100%', height: '100%' })
      }.tabBar(this.bottomBarBuilder('主页', $r('app.media.index'), $r('app.media.indexgray'), 0))

      TabContent() {
        Column() {
          if (this.RecordDataArray.length != 0) {
            RecordGrid()
          }
          else {
            Text('暂无运动记录').fontSize(19).width('100%').height(20).margin({ top: 12, left: 20 })
          }
        }
        .width('100%')
        .height('100%')
        .backgroundImage($r('app.media.background2'))
        .backgroundImageSize({ width: '100%', height: '100%' })

      }.tabBar(this.bottomBarBuilder('记录', $r('app.media.statistics'), $r('app.media.statisticsgray'), 1))
    }
    .onChange((index: number) => {
      this.currentIndex = index;
    })
  }
}

@Component
struct RecordGridItem {
  @State RecordDataArray: Array<RecordData> = RecordDataArray
  @State RecordSports: Array<SportsData> = RecordSports
  private recordItem: RecordData
  private controller: CustomDialogController
  private sportsItem: SportsData
  private item_index: number

  aboutToAppear() {
    this.controller = new CustomDialogController({
      builder: Record({ sportsItem: this.sportsItem, title: '修改运动', mode: 1, item_index: this.item_index }),
      alignment: DialogAlignment.Center
    })
  }

  build() {
    Row() {
      Image(this.sportsItem.image)
        .objectFit(ImageFit.Contain)
        .height(60)
        .width(60)
        .backgroundColor(Color.White)
        .borderRadius(12)
        .margin(5)
        .backgroundColor(Color.White)

      Column({ space: 6 }) {
        Text(this.recordItem.name).fontSize(20).fontWeight(400)
        Text(this.recordItem.time + '分钟').fontSize(18).fontColor(Color.Gray)
      }.alignItems(HorizontalAlign.Start).width(150).margin({ left: 10 })

      Blank()


      Text(this.recordItem.sum.toString() + '千卡').fontSize(18).fontColor('#5C5C5C').width(100).padding(5)
    }
    .width('100%')
    .margin({ left: 10, top: 20 })
    .onClick(() => {
      this.controller.open()
    })
  }
}

@Component
export struct RecordGrid {
  @State RecordDataArray: Array<RecordData> = RecordDataArray
  @State RecordSports: Array<SportsData> = RecordSports

  build() {
    Scroll() {
      List({ space: 20 }) {
        ForEach(this.RecordDataArray, (item: RecordData, index: number) => {
          ListItem() {
            RecordGridItem({ recordItem: item, sportsItem: this.RecordSports[index], item_index: index })
          }
        })
      }.height('100%')
    }
    .scrollBar(BarState.Off)
  }
}

@Component
struct SportsCategory {
  private sportsItems: SportsData[]
  @State currentIndex: number = 0

  @Builder tabBuilder(index: number, name: string) {
    Flex({ justifyContent: FlexAlign.Center, direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text(name).fontSize(20).fontColor(Color.White)
    }
    .borderRadius(15)
    .width(90)
    .height(30)
    .backgroundColor(this.currentIndex === index ? '#3ECF69' : '#bfbfbf')
  }

  build() {
    Stack() {
      Tabs({ barPosition: BarPosition.Start }) {
        TabContent() {
          SportsGrid({ sportsItems: this.sportsItems.filter(item => (item.category === Category.Walking)) })
        }.tabBar(this.tabBuilder(0, '走路'))

        TabContent() {
          SportsGrid({ sportsItems: this.sportsItems.filter(item => (item.category === Category.Running)) })
        }.tabBar(this.tabBuilder(1, '跑步'))

        TabContent() {
          SportsGrid({ sportsItems: this.sportsItems.filter(item => (item.category === Category.Swimming)) })
        }.tabBar(this.tabBuilder(2, '游泳'))

        TabContent() {
          SportsGrid({ sportsItems: this.sportsItems.filter(item => (item.category === Category.Riding)) })
        }.tabBar(this.tabBuilder(3, '骑行'))

        TabContent() {
          SportsGrid({ sportsItems: this.sportsItems.filter(item => (item.category === Category.Ball)) })
        }.tabBar(this.tabBuilder(4, '打球'))

        TabContent() {
          SportsGrid({ sportsItems: this.sportsItems.filter(item => (item.category === Category.Other)) })
        }.tabBar(this.tabBuilder(5, '其他'))
      }
      .vertical(true)
      .barHeight('70%')
      .barWidth(100)
      .onChange((index: number) => {
        this.currentIndex = index
      })
    }
  }
}

@Component
export struct SportsGrid {
  private sportsItems: SportsData[]
  build() {
    Scroll() {
      List({ space: 20 }) {
        ForEach(this.sportsItems, (item: SportsData) => {
          ListItem() {
            SportsGridItem({ sportsItem: item })
          }
        }, (item: SportsData) => item.id.toString())
      }.height('100%')
    }
    .scrollBar(BarState.Off)
  }
}

@CustomDialog
export struct Record {
  @State RecordDataArray: Array<RecordData> = RecordDataArray
  @State RecordSports: Array<SportsData> = RecordSports
  private title: string = '添加运动'
  private sportsItem: SportsData
  private item_index: number
  private mode: number = 0 //0:添加,1:修改
  private controller: CustomDialogController
  @State time: string = '0'
  @State sum: number = 0
  private Valueinput: any[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '删除记录', '0', $r('app.media.Back')]

  calculate() {
    if (this.time.length != 0) {
      this.sum = Math.round(parseInt(this.time) * this.sportsItem.value / 60)
    } else {
      this.sum = 0
    }
  }

  @Builder RecordItem(image: Resource, name: string, value: number) {
    Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center }) {
      Image(image).width(60).height(60).borderRadius(30).objectFit(ImageFit.Contain).backgroundColor('#ffffff').margin(5)
      Text(name).fontSize(17).margin(5)
      Row({ space: 6 }) {
        Text(value.toString()).fontColor('#EC7143').fontSize(19)
        Text('千卡/60分钟').fontColor(Color.Gray).fontSize(17)
      }.margin(5)
    }.height(170).backgroundColor('#F5F5F5').width('100%')
  }

  @Builder valueInput() {
    Column() {
      Grid() {
        ForEach(this.Valueinput, (item: any) => {
          GridItem() {
            if (typeof (item) == 'string') {
              Text(item)
                .fontSize(20)
                .fontWeight(500)
                .width('100%')
                .height('100%')
                .textAlign(TextAlign.Center)
                .onClick(() => {
                  if(item.length<2){
                    if (this.time == '0') {
                      this.time = item
                    }
                    else if (parseInt(this.time) < 999 && parseInt(this.time + item) < 999) {
                      this.time = this.time + item
                    }
                    else {
                      this.time = '999'
                    }
                    this.calculate()
                  }
                  else{
                    if(this.mode==1){
                      AlertDialog.show(
                        {
                          message: '确认删除这条运动记录吗？',
                          primaryButton: {
                            value: '取消',
                            action: () => {
                            }
                          },
                          secondaryButton: {
                            value: '确定',
                            action: () => {
                              RecordDataArray.splice(this.item_index, 1)
                              RecordSports.splice(this.item_index, 1)
                              this.controller.close()
                            }
                          },
                          cancel: () => {
                          }
                        }
                      )
                    }
                  }
                })

            } else if (typeof (item) == 'object') {
              Image(item).width(20).aspectRatio(1).objectFit(ImageFit.Contain)
                .onClick(() => {
                  if (this.time.length > 1) {
                    this.time = this.time.substring(0, this.time.length - 1)
                  }
                  else if (this.time.length == 1) {
                    this.time = '0'
                  }
                  this.calculate()
                })
            }
          }

        })
      }
      .backgroundColor(Color.White)
      .columnsTemplate('1fr 1fr 1fr')
      .rowsTemplate('1fr 1fr 1fr 1fr')
      .columnsGap(0)
      .rowsGap(0)
      .width('100%')
      .height('35%')
    }
  }

  build() {
    Column() {
      Row() {
        Button('取消').fontSize(16).fontColor('#3ECF69').width(70).height(25).backgroundColor(Color.White)
          .onClick(() => {
            this.controller.close()
          })
        Blank().flexGrow(1)
        Text(this.title).fontSize(22).fontWeight(400).width(100)
        Blank().flexGrow(1)
        Button('确认').fontSize(16).fontColor('#3ECF69').width(70).height(25).backgroundColor(Color.White)
          .onClick(() => {
            if (this.time == '0') {
              prompt.showToast({ message: '输入不能为零', duration: 1000 })
            }
            else {
              if (this.mode == 0) {
                RecordDataArray.push({
                  'name': this.sportsItem.name,
                  'image': this.sportsItem.image,
                  'time': this.time,
                  'sum': this.sum
                })
                RecordSports.push(this.sportsItem)
              }
              else if (this.mode == 1) {
                RecordDataArray.splice(this.item_index, 1)
                RecordSports.splice(this.item_index, 1)
                RecordDataArray.push({
                  'name': this.sportsItem.name,
                  'image': this.sportsItem.image,
                  'time': this.time,
                  'sum': this.sum
                })
                RecordSports.push(this.sportsItem)

              }
              this.controller.close()
            }

          })
      }.width('100%').height(50)

      Divider().width('100%').vertical(false).strokeWidth(0.5)

      this.RecordItem(this.sportsItem.image, this.sportsItem.name, this.sportsItem.value)

      Divider().width('100%').vertical(false).strokeWidth(0.5)

      Row() {
        Blank().flexGrow(100)
        Row() {
          Text(this.time).fontColor('#E14843').fontSize(28).margin({ right: 3 })
          Text('分钟').fontSize(17)
        }.width(100)

        Blank().flexGrow(1)
        Text(this.sum + '千卡').fontColor(Color.Gray).fontSize(17).width(100).textAlign(TextAlign.End).padding(5)
      }.width('100%').margin({ top: 12, bottom: 12 })

      Divider().width('100%').vertical(false).strokeWidth(0.5)
      this.valueInput()
    }.width('82%')
  }
}

@Component
export struct SportsGridItem {
  private sportsItem: SportsData
  private controller: CustomDialogController

  aboutToAppear() {
    this.controller = new CustomDialogController({
      builder: Record({ sportsItem: this.sportsItem }),
      alignment: DialogAlignment.Center
    })
  }

  @Builder sportsDetail(value: number) {
    Row({ space: 10 }) {
      Text(value.toString()).fontColor('#E14843').fontSize(16)
      Text('千卡/60分钟').fontColor(Color.Gray).fontSize(16)
    }
  }

  build() {
    Row({ space: 10 }) {
      Image(this.sportsItem.image)
        .objectFit(ImageFit.Contain)
        .height(60)
        .aspectRatio(1)
        .borderRadius(12)
        .margin(5)
        .backgroundColor(Color.White)

      Column({ space: 6 }) {
        Text(this.sportsItem.name).fontSize(19).fontWeight(500)
        this.sportsDetail(this.sportsItem.value)
      }.alignItems(HorizontalAlign.Start)
    }
    .margin({ left: 10, top: 20 })
    .onClick(() => {
      this.controller.open()
    })
  }
}


