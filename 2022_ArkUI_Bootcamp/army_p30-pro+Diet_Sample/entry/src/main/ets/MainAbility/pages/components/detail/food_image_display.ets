import { FoodInfo } from '../../../model/type/DietType'
import curves from '@ohos.curves'

@Preview
@Component
export struct FoodImageDisplay {
    private foodInfo: FoodInfo
    @State imageBgColorA: number = 0
    @StorageProp('currentBreakpoint') currentBreakpoint: string = 'sm'

    build() {
        Stack({ alignContent: Alignment.BottomStart }) {
            Image(this.foodInfo.image)
                .sharedTransition(this.foodInfo.letter, {
                    duration: 400,
                    curve: curves.cubicBezier(0.2, 0.2, 0.1, 1.0),
                    delay: 100
                })
                .backgroundColor(`rgba(255, 255, 255, ${this.imageBgColorA})`)
                .objectFit(ImageFit.Contain)
            Text(this.foodInfo.name)
                .fontSize(26)
                .fontWeight(FontWeight.Bold)
                .margin({ left: 26, bottom: 18 })
        }
        .height(this.currentBreakpoint == 'lg' ? 166 : 280)
    }
}
