import Curves from '@ohos.curves'
import router from '@ohos.router'

@Entry
@Component
struct Animation {
  @State private opacityValue: number = 0
  @State private scaleValue: number = 0
  private curve1 = Curves.cubicBezier(0.4, 0, 1, 1)
  private pathCommands1: string = 'M524.2 653h-0.7c-165.8-2.4-298.9-70.3-365.3-186.3-65.4-114.5-54.8-270.4 28.6-417.2 7.8-13.6 22.9-21.9 38.1-21.8C342.6 29.4 444.3 68.8 519 141.4c17.1 16.6 17.5 43.9 0.8 61-16.6 17-43.9 17.5-61 0.8-53.3-51.8-125.3-82.1-209.5-88.2-57 112.7-63.4 226.7-16.4 308.8 50.9 88.9 157.2 141 291.7 142.9 23.8 0.3 42.8 19.9 42.5 43.7-0.2 23.7-19.4 42.6-42.9 42.6z'
  private pathCommands2: string = 'M531.9 651.5c-13.4 0-26.1-6.2-34.3-17-99-130.3-124.5-272.3-71.9-399.7C477.9 108.5 602.1 23.1 766.4 0.5c15.5-2.4 30.8 4.2 40.2 16.6 100.4 132.1 128 280.2 75.8 406.4-50.2 121.3-175.8 204.3-344.7 227.6-1.8 0.2-3.8 0.4-5.8 0.4z m-26.5-383.8c-37.9 91.6-21.5 195.1 45.9 293.9 82.7-15.2 204.3-57 251.4-171C840.3 299.7 822 191.3 752.9 90 632.5 112.9 543 176.8 505.4 267.7z'
  private pathCommands3: string = 'M336.4 870.3c-7.1 0-14.2-1.7-20.8-5.4-35.3-19.5-67.3-44.3-95.2-73.5-16.4-17.2-15.8-44.5 1.4-60.9 17.2-16.5 44.5-15.9 61 1.4 21.9 22.9 47 42.3 74.6 57.6 20.8 11.5 28.4 37.8 16.8 58.6-7.9 14.2-22.6 22.2-37.8 22.2z m349.2-3.2c-14.9 0-29.4-7.7-37.4-21.5-11.9-20.6-4.9-47 15.7-58.9 25.7-14.9 49.2-33.3 69.7-54.8 16.5-17.2 43.7-17.8 60.9-1.3 17.2 16.5 17.8 43.7 1.4 61-26.1 27.4-56.1 50.9-88.9 69.8-6.7 3.9-14.1 5.7-21.4 5.7z'
  private pathCommands4: string = 'M508.2 1024c-23.8 0-43.1-19.3-43.1-43.1V702.4c0-23.8 19.3-43.1 43.1-43.1s43.1 19.3 43.1 43.1v278.5c0 23.8-19.3 43.1-43.1 43.1z'

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Shape() {
        Path()
          .commands(this.pathCommands1)
          .fill(Color.White)
        Path()
          .commands(this.pathCommands2)
          .fill(Color.White)
        Path()
          .commands(this.pathCommands3)
          .fill(Color.White)
        Path()
          .commands(this.pathCommands4)
          .fill(Color.White)
      }
      .scale({ x: this.scaleValue, y: this.scaleValue })
      .opacity(this.opacityValue)
      .onAppear(() => {
        animateTo({
          duration: 2000,
          curve: this.curve1,
          delay: 100,
          onFinish: () => {
            setTimeout(() => {
              router.replace({ url: "pages/Logo" })
            }, 1000);
          }
        }, () => {
          this.opacityValue = 1
          this.scaleValue = 1
        })
      })

      Text('Healthy Diet')
        .fontSize(26)
        .fontColor(Color.White)
        .margin({ top: 300 })

      Text('Healthy life comes from a balanced diet')
        .fontSize(17)
        .fontColor(Color.White)
        .margin({ top: 4 })
    }
    .width('100%')
    .height('100%')
    .linearGradient(
      {
        angle: 180,
        colors: [['#BDE895', 0.1], ["#95DE7F", 0.6], ["#7AB967", 1]]
      })
  }
}