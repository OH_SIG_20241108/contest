import { DeviceTitle, DeviceLogo, DeviceState, ProgressBar, Stow, componentsRow } from '../utils/componentUtil';

/**
 * 窗帘控制界面
 */
@Entry
@Component
struct CurtainDeviceControl {
  scroller: Scroller = new Scroller()
  @StorageLink('curtainState') isON: boolean = false
  deviceLogo: Resource = $r("app.media.icon_curtain")
  deviceSwitchText: Resource = $r("app.string.closed")
  deviceSwitchTextColor = $r("app.color.background_black")
  @State switchImg: Resource = $r("app.media.icon_switch_off")
  @State progressValue: number = 50
  openStateImage: Resource = $r("app.media.icon_curtain_open_unselected")
  closeStateImage: Resource = $r("app.media.icon_curtain_close_selected")

  aboutToAppear() {
    this.isON = AppStorage.Get('curtainState')
    this.setCommandMessage()
  }

  setCommandMessage() {
    if (this.isON) {
      this.openStateImage = $r("app.media.icon_curtain_open_selected")
      this.closeStateImage = $r("app.media.icon_curtain_close_unselected")
      this.switchImg = $r("app.media.icon_switch_on")
      this.deviceSwitchText = $r("app.string.opened");
    } else {
      this.openStateImage = $r("app.media.icon_curtain_open_unselected")
      this.closeStateImage = $r("app.media.icon_curtain_close_selected")
      this.switchImg = $r("app.media.icon_switch_off")
      this.deviceSwitchText = $r("app.string.closed");
    }
  }

  build() {
    Flex({ direction: FlexDirection.Column }) {
      DeviceTitle({ deviceTitle: $r("app.string.smartCurtain") })
      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center }) {
        DeviceLogo({ deviceLogo: this.deviceLogo })
        Scroll(this.scroller) {
          Column({ space: 12 }) {
            DeviceState({
              deviceSwitchText: this.deviceSwitchText,
              deviceSwitchTextColor: this.deviceSwitchTextColor,
              isAlert: true,
              switchImg: this.switchImg,
              callback: () => {
                this.isON = !this.isON
                this.setCommandMessage()
                AppStorage.SetOrCreate<boolean>('curtainState', this.isON)
              }
            })

            Flex({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceBetween }) {
              componentsRow({
                text: "打开",
                textSize: 16,
                image: this.openStateImage,
                frameWidth: '48%',
              })
              componentsRow({
                text: "关闭",
                textSize: 16,
                image: this.closeStateImage,
                frameWidth: '48%',
              })
            }
            .height(64)
            .margin({ top: 12 })
            .width('100%')

            ProgressBar({
              text: "开合度",
              textSize: 16,
              textProgress: $progressValue,
              textProgressSize: 12,
              progressWidth: '80%',
              isSendCommand: true,
            })

            Stow()
            Flex({ direction: FlexDirection.Row, justifyContent: FlexAlign.SpaceBetween }) {
              componentsRow({
                text: $r("app.string.moreProducts"),
                textSize: 14,
                image: $r("app.media.icon_more_device"),
                frameWidth: '48%',
              })
              componentsRow({
                text: $r("app.string.partsReplacement"),
                textSize: 14,
                image: $r("app.media.icon_replace"),
                frameWidth: '48%',
              })
            }
            .height(64)
            .margin({ top: 12 })
            .width('100%')
          }
        }
      }
    }
    .backgroundColor($r("app.color.background_grey"))
    .padding({ left: 16, right: 16 })
  }
}