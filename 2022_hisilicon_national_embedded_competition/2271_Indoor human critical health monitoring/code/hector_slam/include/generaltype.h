#ifndef _GENERALTYPE_H
#define _GENERALTYPE_H

#include<vector>

namespace HiRobot{
struct Quat
{
    float w;
    float x;
    float y;
    float z;
};

struct Coor3d
{
    float x;
    float y;
    float z;
};

struct RobotPose{
  Quat orientation;
  Coor3d position;
};

struct LaserScan{
    std::vector<float> ranges;
    float range_max;
    float range_min;
    float angle_max;
    float angle_min;
    float angle_increment;
};

}

#endif
