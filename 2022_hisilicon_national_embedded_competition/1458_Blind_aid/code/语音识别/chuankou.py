import serial
ser=serial.Serial()
def port_open_recv():
    ser.port='com5'#定义串口号
    ser.baudrate=9600
    ser.bytesize=8
    ser.stopbits=1
    ser.parity="N"
    ser.open()
    if (ser.isOpen()):
        print("串口打开成功")
    else:
        print("串口打开失败")

def port_close():
    ser.close()
    if(ser.isOpen()):
        print("串口关闭失败")
    else:
        print("串口关闭成功")

def send(send_data):
    if(ser.isOpen()):
        ser.write(send_data)
        print("发送成功")
    else:
        print("发送失败，串口没有打开")





