#ifndef APP_MQTT_IOT_H
#define APP_MQTT_IOT_H

#include <hi_stdlib.h>
#include <hi_gpio.h>
#include <hi_io.h>
#include "iot_gpio_ex.h"
#include "ohos_init.h"
#include "cmsis_os2.h"

static void DemoMsgRcvCallBack(int qos, const char *topic, const char *payload);

/* 反馈给小程序是否成功开启跟随 */
hi_void IotPublishFollowStatus(void);

/* 上传商品信息 */
hi_void IotPublishNewItem(void);

#endif