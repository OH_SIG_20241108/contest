/*
 * @Author: Shaohan Weng 
 * @Email: shaohanweng@outlook.com
 * @Description: 程序入口
 * 源码主体部分位于 ive_thresh.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "sdk.h"
#include "ive_thresh.h"

static char **s_ppChCmdArgv = NULL;

/* function : to process abnormal case */
#if (!defined(__HuaweiLite__)) || defined(__OHOS__)
static HI_VOID IVE_HandleSig(HI_S32 s32Signo)
{
    if (s32Signo == SIGINT || s32Signo == SIGTERM) {
        IVE_Thresh_HandleSig();
    }
}
#endif

/* function : main */
#if defined(__HuaweiLite__) && (!defined(__OHOS__))
int app_main(int argc, char *argv[])
#else
int main(int argc, char *argv[])
#endif
{
    int ret = HI_SUCCESS;
    sdk_init();
    s_ppChCmdArgv = argv;
#if (!defined(__HuaweiLite__)) || defined(__OHOS__)
    struct sigaction sa;
    (hi_void)memset_s(&sa, sizeof(struct sigaction), 0, sizeof(struct sigaction));
    sa.sa_handler = IVE_HandleSig;
    sa.sa_flags = 0;
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
#endif
    IVE_Thresh();
end:
    sdk_exit();
    return ret;
}
