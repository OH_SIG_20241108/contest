#ifndef MY_REGION_COUNT_H
#define MY_REGION_COUNT_H


#include "my_yolo_process.h"


#define REGIONNUM 8
#define FLAG_REGIONNUM 8

#define REGION_1_XMIN 
typedef struct SequencePtr {
    int *regionPtr_0;
    int *regionPtr_1;
    int *regionPtr_2;
} SequencePtr;

void MyRegionCount(const RectBox *boxes, int objNum, int regionArr[]);

void MyRegionAbstract(const int *regionArr, unsigned char regionAbstract[]);

void MyRegionAbstract2(const int *regionArr, unsigned char regionAbstract[]);

void MyGetRegion(RectBox regionBoxes[]);

void MyGetRegion2(const RectBox *anchors, const int *anchorIdx, RectBox regionBoxes[]);

void MyRegionCount2(const RectBox *carBoxes, const RectBox *regionBoxes, int carNum, int regionArr[]);

#endif
