/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <hi_time.h>
#include <hi_watchdog.h>
#include "iot_errno.h"
#include "iot_gpio.h"
#include "ssd1306_oled.h"
#include "app_demo_i2c_oled.h"
#include "iot_gpio_ex.h"
#include "app_demo_multi_sample.h"

#define DELAY_TIMES_100 100
#define DELAY_TIMES_5   5

#define PWM_FRQ_50      50
#define PWM_FRQ_10      10
#define PWM_FRQ_100     100
#define PWM_FRQ_20      20

#define TICK_COUNT_MAX  255
#define PAIR_2          2

GlobalStausType globalStaType = {0};

/* Set key status */ //把按键状态设置为 ‘setSta’
unsigned char SetKeyStatus(HiTrafficLightMode setSta)
{
    globalStaType.g_currentMode = setSta;   //globalStaType是个结构体
    return globalStaType.g_currentMode;     //成员是“当前模式”、“当前状态”、“按键标志”等一些全局状态
}

/* Set key type */
unsigned char SetKeyType(HiControlModeType setSta)
{
    globalStaType.g_currentType = setSta;
    return globalStaType.g_currentType;
}

/* GetKeyStatus */
unsigned char GetKeyStatus(GloableStatuDef staDef)
{
    unsigned char status = 0;
    switch (staDef) 
    {
        // case MENU_SELECT:
        //     status = globalStaType.g_menuSelect;
        //     break;
        // case MENU_MODE:
        //     status = globalStaType.g_menuMode;
        //     break;
        case CURRENT_MODE:
            status = globalStaType.g_currentMode;
            break;
        case CURRENT_TYPE:
            status = globalStaType.g_currentType;
            break;
        // case KEY_DOWN_FLAG:
        //     status = globalStaType.g_keyDownFlag;
        //     break;

        default:
            break;
    }
    return status;
}

/* factory test HiSpark board */ //板测程序，暂时不知道有没有用
void HisparkBoardTest(IotGpioValue value)
{
    IoSetFunc(HI_GPIO_9, 0); // GPIO9
    IoTGpioSetDir(HI_GPIO_9, IOT_GPIO_DIR_OUT); // GPIO9
    IoTGpioSetOutputVal(HI_GPIO_9, value); // GPIO9
}

/* gpio init */
void GpioControl(unsigned int gpio, unsigned int id, IotGpioDir dir, IotGpioValue  gpioVal,
                 unsigned char val)
{
    IoSetFunc(gpio, val);
    IoTGpioSetDir(id, dir);
    IoTGpioSetOutputVal(id, gpioVal);
}
/* pwm init */
void PwmInit(unsigned int id, unsigned char val, unsigned int port)
{
    IoSetFunc(id, val); /* PWM0 OUT */
    IoTPwmInit(port);
}




