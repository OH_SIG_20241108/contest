#ifndef __SAMPLE_LCD_H__
#define __SAMPLE_LCD_H__


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */


//sample_region.h start
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>

#include "sample_comm.h"
#include "mpi_snap.h"
#include "hi_comm_region.h"
#include "hi_common.h"
#include "hi_comm_vo.h"


#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#ifdef __HuaweiLite__
#define  RES_BMP  "/sharefs/bmp/"
#else
#define  RES_BMP  "./"
#endif

//#define  vi_chn_0_bmp  RES_BMP"save.bmp"
//extern HI_CHAR*    Path_BMP;
//sample_region.h end


#ifndef SAMPLE_PRT
#define SAMPLE_PRT(fmt...)   \
    do {\
        printf("[%s]-%d: ", __FUNCTION__, __LINE__);\
        printf(fmt);\
    }while(0)
#endif

#ifndef PAUSE
#define PAUSE()  do {\
        printf("---------------press Enter key to exit!---------------\n");\
        getchar();\
    } while (0)

#endif
  
HI_S32 SAMPLE_VIO_VPSS_VO_MIPI(HI_VOID);
HI_S32 SAMPLE_COMM_VO_StartDev_MIPI(VO_DEV VoDev, VO_PUB_ATTR_S* pstPubAttr);
//HI_S32 SAMPLE_COMM_REGION_SetBitMap(RGN_HANDLE Handle,PIXEL_FORMAT_E enPixelFmt);
//HI_S32 SAMPLE_CREATE_OSD();


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

#endif /* End of #ifndef __SAMPLE_LCD_H__*/
