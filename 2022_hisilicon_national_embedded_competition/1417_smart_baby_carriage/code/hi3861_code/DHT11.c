#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "oled_ssd1306.h"
#include "DHT11.h"
// 6 7 9 11


static void DHT11Task(void)
{
    int readflag;
    uint8_t data[2];
    DHT11_RST();
    printf("DHT11 RST OK!\r\n");
    while(DHT11_Init());
    printf("DHT11 Init() OK!\r\n");
    while(1)
    {
        data[0]=0;
        data[1]=0;
        printf("VVVVVVVVVVVVVVVVVVVVVVVVVV\r\n");
        readflag=DHT11_Read_Data(data);
        printf("readflag=%dVVVVVVVVVVVVVVVVVVVVVVVVVV\r\n",readflag);
        if(readflag==0)
        {
        //printf("VVVVVVVVVVVVVVVVVVVVVVVVVV\r\n");
        printf("tempretuuer=%dV\n",data[0]);
        printf("humi=%dV\n",data[1]);
        usleep(1000000);//每延时1s输出一次电压情况
        }
    }
}
static void DHT11text(void)//adc测试程序
{
    osThreadAttr_t attr;
    attr.name = "DHT11Task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /* 任务大小4096 */
    attr.priority = osPriorityNormal;
    if (osThreadNew(DHT11Task, NULL, &attr) == NULL) {
        printf("[DHT11Task] Falied to create ADCTask!\n");
    }
}
APP_FEATURE_INIT(DHT11text);