
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

def PicEncrypt(PicPath):
    password = b'uestc11451419198' 
    
    iv = b'qwqqvq1231321321'
    print(len(iv))
    with open(PicPath,'rb') as f:
        pic = f.read()
    PadPic = pad(pic, AES.block_size)
    aes = AES.new(password,AES.MODE_CBC,iv)
    # AES.MODE_CBC 表示模式是CBC模式
    en_text = aes.encrypt(PadPic) 
    with open(PicPath + '.aes','wb+') as f:
        f.write(en_text)
    

def PicDecrypt(PicPath):
    password = b'uestc11451419198' 
    
    iv = b'qwqqvq1231321321'
    print(len(iv))
    with open(PicPath + '.aes','rb') as f:
        pic = f.read()
    aes = AES.new(password,AES.MODE_CBC,iv) 
    DePic = aes.decrypt(pic)

    OriPic = unpad(DePic, AES.block_size)
    with open(PicPath,'wb+') as f:
        f.write(OriPic)
    
PicEncrypt('know/lx1.jpg')
PicEncrypt('know/lbw1.jpg')
PicDecrypt('know/lx1.jpg')
PicDecrypt('know/lbw1.jpg')
