// pages/storehouse/dashdetail/dashdetail.js
const app = getApp()

Page({
  data: {
    data : [],
    flag:''
  },
  onLoad: function (options) {
    console.log(options)
    var that = this
    var flag = options.flag;
    that.setData({
      flag: flag
    })
    wx.request({
      url: 'https://dlut.cpolar.cn/get_time_data',
      data:{
        flag:String(flag)
      },
      success: function (res) {
        console.log(res.data)
        that.setData({
          data:res.data.ans
        })
      }
    })
  },
})