Page({

  /**
   * 页面的初始数据
   */
  data: {
    face: '',
    mask: '',
    gender:0,
    name:'',
    id:'',
    answer:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  facepic: function () {
    var that = this
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        console.log(res)
        var base = wx.getFileSystemManager().readFileSync(res.tempFilePaths[0], "base64")
        wx.request({
          url: 'https://dlut.cpolar.cn/url',
          data: {
            base64: base
          },
          success: function (res) {
            that.setData({
              face: res.data.ans
            })
            var url = res.data.ans
            console.log(url)
          }
        })
      },
      fail: e => {
        console.error(e)
      }
    })
  },
  maskpic: function () {
    var that = this
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        console.log(res)
        var base = wx.getFileSystemManager().readFileSync(res.tempFilePaths[0], "base64")
        wx.request({
          url: 'https://dlut.cpolar.cn/url',
          data: {
            base64: base
          },
          success: function (res) {
            that.setData({
              mask: res.data.ans
            })
            var url = res.data.ans
            console.log(url)
          }
        })
      },
      fail: e => {
        console.error(e)
      }
    })
  },
  name: function (e) {
    console.log(e.detail.value)
    this.setData({
      name: e.detail.value //待插入的title字段
    })
  },
  id: function (e) {
    console.log(e.detail.value)
    this.setData({
      id: e.detail.value //待插入的title字段
    })
  },
  Gender(e) {
    console.log(e.detail.value)
    this.setData({
      gender: e.detail.value
    })
  },
  add:function() {
    var that=this
    wx.request({
      url: 'https://dlut.cpolar.cn/add',
      data: {
        face:that.data.face,
        mask:that.data.mask,
        name:that.data.name,
        id:that.data.id,
        gender:that.data.gender
      },
      success: function (res) {
        that.setData({
          answer: res.data.ans
        })
        var answer = res.data.ans
        console.log(answer)
        if(answer=="成功"){
          wx.navigateBack({
            delta: 0,
          })
          wx.showToast({
            title: answer,
          })
        } else {
          wx.showToast({
            title: answer,
            icon: 'none',
          })
        }
      }
    })
  }
})