#ifndef __FLOWER_DATA_H
#define __FLOWER_DATA_H

#include <stdint.h>

typedef enum{
    MARIGOLD,                   //金盏花
    AZALEA,                     //杜鹃
    POINSETTIA,                 //一品红
    BUTTERFLY_ORCHID,           //蝴蝶兰
    HYACINTH,                   //风信子
    UNKNOWN,                    //未知
}Flower_Type;

static const float flower_temp[][2] = {
    {7.0,20.0},                //金盏花
    {12.0,25.0},                //杜鹃
    {18.0,26.0},                //一品红
    {16.0,30.0},                //蝴蝶兰
    {20.0,30.0},                //风信子
    {0.1 ,100.0},
};

static const float flower_ph[][2] = {
    {6.0,7.0},                  //金盏花
    {5.5,6.5},                  //杜鹃
    {5.5,6.5},                  //一品红
    {6.0,7.0},                  //蝴蝶兰
    {6.0,7.0},                  //风信子
    {0.1,12.3},
};

static const float flower_hum[][2] = {
    {75.0,85.0},                 //金盏花
    {60.0,80.0},                 //杜鹃
    {65.0,80.0},                 //一品红
    {70.0,80.0},                 //蝴蝶兰
    {60.0,70.0},                 //风信子
    {0.1 ,100.0},
};

uint8_t temp_func(uint8_t type,float temp);
uint8_t ph_func(uint8_t type,float ph);
uint8_t hum_func(uint8_t type,float hum);
float temp_mim(uint8_t type);
float temp_max(uint8_t type);
float ph_mim(uint8_t type);
float ph_max(uint8_t type);
float hum_mim(uint8_t type);
float hum_max(uint8_t type);

#endif