import cv2

def GetPicture():
    """
    拍照保存图像
    :return:
    """
    # 创建一个窗口
    cv2.namedWindow('camera', 1)
    # 调用摄像头   IP摄像头APP
    # video = "http://admin:admin@192.168.3.32:8081/video"
    video = "http://swz:swz273220@192.168.31.108:8081/video"
    cap = cv2.VideoCapture(video)
    while True:
        success, img = cap.read()
        cv2.imshow("camera", img)
        # 按键处理
        key = cv2.waitKey(10)
        if key == 27:
            # esc
            break
        #for i in range(100):
        if key == 32:
            # 空格
            # fileaname = 'frames'+i+'.jpg'
            fileaname = 'frames.jpg'
            cv2.imwrite(fileaname, img)

    # 释放摄像头
    cap.release()
    # 关闭窗口
    cv2.destroyWindow("camera")

if __name__ == "__main__":
    GetPicture()