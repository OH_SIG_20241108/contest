# code重点代码讲解：
## mmclassification部分：
hello.py是对训练的图片的名字进行一个统一转换的一个脚本，我觉得这有利于训练。
resnet18.py和imagenet_bs32.py的改动是为了符合海思Hi3516DV300的硬件要求和训练要求。
## openharmony部分：
### mask_classify文件夹：
mask_classify.c：对垃圾分类的demo进行修改。
mask_classify.h：mask_classify.c所用到的头文件。
### smp文件：
在主函数和头文件中加入mask_classify所需要的函数和头文件。
## YOLOv5部分：
detect.py检测的主函数。
train.py训练的主要函数。
