
#include "sample_store.h"
#include "sdk.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* End of #ifdef __cplusplus */

/*
 * function    : main()
 * Description : main
 */
int main(void)
{
    int ret;
    sdk_init();
    ret = SAMPLE_VENC_H265_H264();
    sdk_exit();
    SAMPLE_PRT("\nsdk exit success\n");
    return ret;
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */
