import router from '@system.router';


export default {
    data: {
        title: "唐诗宋词",
        red_color: '#ff0000',
        green_Color: '#00ff00',
        swipe_index:1,

    },
    ToHomePage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/jscontrol/jscontrol',
        });
    },
    ToGamePage(){
        this.work_status ="寓教于乐界面"
        router.replace({
            uri: 'pages/game_page/game_page',
        });
    },

}