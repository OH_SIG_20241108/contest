import router from '@system.router';


export default {
    data: {
        title: "Hello World",
        word_list: [
            {
                title: 'Solar System',
                subtitle: '长',
                img:'/common/images/solar-system.png',
            },
            {
                title: 'Mercury',
                subtitle: '按',
                img:'/common/images/mercury.jpg',
            },
            {
                title: 'Venus',
                subtitle: '即',
                img:'/common/images/venus.jpg',
            },
            {
                title: 'Earth',
                subtitle: '可',
                img:'/common/images/earth.jpg',
            },
            {
                title: 'Mars',
                subtitle: '看',
                img:'/common/images/mars.jpg',
            },
            {
                title: 'Jupiter',
                subtitle: '全',
                img:'/common/images/jupiter.jpg',
            },
            {
                title: 'Saturn',
                subtitle: '部',
                img:'/common/images/saturn.jpg',
            },
            {
                title: 'Uranus',
                subtitle: '答',
                img:'/common/images/uranus.jpg',
            },
            {
                title: 'Neptune',
                subtitle: '案',
                img:'/common/images/neptune.png',
            },
        ],

    },
    ToHomePage(){
        this.work_status ='退出';
        router.replace({
            uri: 'pages/jscontrol/jscontrol',
        });
    },
    ToGamePage(){
        this.work_status ="寓教于乐界面"
        router.replace({
            uri: 'pages/game_page/game_page'
        })
    },
    Answer()
    {
        this.word_list[0].subtitle='鸿蒙';
        this.word_list[1].subtitle='水星';
        this.word_list[2].subtitle='金星';
        this.word_list[3].subtitle='地球';
        this.word_list[4].subtitle='火星';
        this.word_list[5].subtitle='木星';
        this.word_list[6].subtitle='土星';
        this.word_list[7].subtitle='天王星';
        this.word_list[8].subtitle='海王星';
    }

}