/**
 * Js控制主界面  完全实现交互数据下发至机器人端
 * @ HelloKun 2022.04
 */

//弹窗
import {getApp} from '../../common.js';
import prompt from '@system.prompt';

export default {

    data: {
        title: "你好,我是机器人小萌",
        work_status:"点击右侧按钮可以控制我哦",
        emoji:"表情聊天",
        talk_msg:"对萌萌说点什么",
        fontSize: '30px',
        Pink_Color: '#ffc0cb',
        White_color:'#ffffff',
        Black_color:'#000000',
        Yellow_Color: '#ffff00',
        Introduction:'我是小萌，\n我能说话、移动、控制家电等，\n更多功能等你探索，快和我玩吧！\nDesigned By @hellokun.cn\n ',
        front_img:'/common/images/mengmeng.png',
        fan:"/common/images/fanOff.png",
        door:"/common/images/doorLock.png",
        lamb:"/common/images/lambOff.png",
        water:"/common/images/waterOff.png",

    },

    sendMessage(){
        var ret =1;
        var message = this.talk_msg;
        let commonInfo = {
            sessionId: getApp(this).ConfigParams.deviceInfo.sessionId
        };
        getApp(this).NetConfig.sendMessage(commonInfo, message, (result)=>{
                if(result.code==0)
                {
                    prompt.showToast({
                        message: "发送成功",
                        duration: 1000,
                    });
                ret =0;}
                else
                { prompt.showToast({
                        message: "发送失败",
                        duration: 1000,
                    });
               ret= -1;};
            });
        if(ret==0)
        {
            return 0;
        }
        else return -1;
    },


    switchChange(e){
        console.log(e.checked);
        if( e.checked){
            this.talk_msg = "turn on MRobot";
            this.sendMessage();
            console.log("open");
            prompt.showToast({
                message: "连接"
            });
        }
        else{
            this.talk_msg = "reset MRobot";
            this.sendMessage();
            prompt.showToast({
                message: "断开"
            });
        };
    },


    //弹框
    showPrompt(msg) {
        prompt.showToast({
            message: msg,
            duration: 3000,
        });
    },
    ToGamePage(){
        this.work_status ="寓教于乐界面"
        console.log(this.$app.$def.data.game_page);
        //调用全局对象
        this.$app.$def.ToGamePage();

        //全局变量赋值 this.$app.$def.data.game_page = "pages/mathmatic/mathmatic";
        //获取全局变量
        /* var page_game = this.$app.$def.data.game_page;
        router.replace({
            uri:page_game //'pages/game_page/game_page'
        }); */
    },

    //运动控制
    Forward(){
        this.front_img='/common/images/up0.png';
        this.work_status ="萌萌退下";
        this.talk_msg = "F";
        this.sendMessage();
    },
    Backward(){
        this.front_img='/common/images/down0.png';
        this.work_status ="萌萌冲冲冲";
        this.talk_msg = "B";
        this.sendMessage();
    },
    TurnLeft(){
        this.front_img='/common/images/left0.png';
        this.work_status ="右转";
        this.talk_msg = "L";
        this.sendMessage();
    },
    TurnRight(){
        this.front_img='/common/images/right0.png';
        this.work_status ="左转";
        this.talk_msg = "R";
        this.sendMessage();
    },
    RunCircle(){
        this.front_img='/common/images/circle0.png';
        this.work_status ="转圈……";
        this.talk_msg = "C";
        this.sendMessage();
    },
    StopMove(){
        this.front_img='/common/images/left0.png';
        this.work_status ="休息中";
        this.talk_msg = "P";
        this.sendMessage();
    },

    //表情聊天
    Emoji_smile(){
        this.emoji ="微笑";
        this.talk_msg = "1";
       // this.sendMessage();
    },
    Emoji_wao(){
        this.emoji ="哇哦";
        this.talk_msg = "2";
        //this.sendMessage();
    },
    Emoji_haha(){
        this.emoji ="你好";
        this.talk_msg = "3";
        //this.sendMessage();
    },
    Emoji_wuwu(){
        this.emoji ="呜呜";
        this.talk_msg = "4";
       // this.sendMessage();
    },
    Emoji_light(){
        this.emoji ="光环";
        this.talk_msg = "5";
       // this.sendMessage();
    },
    Emoji_red(){
        this.emoji ="红灯停";
        this.talk_msg = "6";
       // this.sendMessage();
    },
    Emoji_yellow(){
        this.emoji ="黄灯等";
        this.talk_msg = "7";
       // this.sendMessage();
    },
    Emoji_green(){
        this.emoji ="绿灯行";
        this.talk_msg = "8";
       // this.sendMessage();
    },


    //秒控设备
    Control_fan(e)
    {
        var ret=0;
        console.log(e.checked);
        if( e.checked){
            this.talk_msg = "turn on fan";
            this.sendMessage();
            this.fan="/common/images/fanOn.png";
            prompt.showToast({
                message: "打开风扇"
            });
        }
        else{
            this.fan ="/common/images/fanOff.png";
            this.talk_msg = "turn off fan";
            this.sendMessage();
            prompt.showToast({
                message: "关闭风扇"
            });
        };
    },
    Control_door(e)
    {
        this.door="/common/images/doorOpen.png";
        if( e.checked){
            this.talk_msg = "open door";
            this.sendMessage();
            console.log("open");
            prompt.showToast({
                message: "打开门锁"
            });
        }
        else{
            this.door="/common/images/doorLock.png";
            this.talk_msg = "close door";
            this.sendMessage();
            prompt.showToast({
                message: "关闭门锁"
            });
        };
    },
    Control_lamb(e)
    {
        this.talk_msg = "turn on lamb";
        this.sendMessage();
        this.lamb="/common/images/lambOn.png";
        if( e.checked){
            console.log("open");
            prompt.showToast({
                message: "打开台灯"
            });
        }
        else{
            this.talk_msg = "turn off lamb";
            this.sendMessage();
            this.lamb="/common/images/lambOff.png";
            prompt.showToast({
                message: "关闭台灯"
            });
        };
    },
    Control_water(e)
    {
        this.talk_msg = "go ahead water";
        this.sendMessage();
        this.water="/common/images/waterOn.png";
        if( e.checked){
            console.log("open");
            prompt.showToast({
                message: "浇水中"
            });
        }
        else{
            this.water="/common/images/waterOff.png";
            this.talk_msg = "stop water";
            this.sendMessage();
            prompt.showToast({
                message: "停止浇水"
            });
        };
    },

    //点击Designed By HelloKun 弹出Dialog
    About_me(){
        this.$element('dialog_about_me').show();
    },
    confirmClick(e) {
        this.$element('dialog_about_me').close()
        prompt.showToast({
            message: '感谢使用'
        })
    },
    About_control(){
        this.$element('dialog_control').show();
    },
}


