// pages/home/home.js
//获取应用实例
const app = getApp();
const API = require('../../utils/api');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 轮播图数据
    swiper: {
      data: [{
          src: "http://photogz.photo.store.qq.com/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzNOTGOgiyvYpspI2v3yVAwP3MddGrJXTvwK9C.fmN1s4nKdagAjzhvBLllENvmClRw!!/m&bo=EwG3ABMBtwARADc!&rf=mood_app",
          goods_id: 1,
          currentTab: -1
        },
        {
          src: "http://photogz.photo.store.qq.com/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzK0gVLoZEKWfUQqBbt7jReIx5TI7VILW1XVgnoZ31BWF9*1h1dcPclp9FTuPbVb1Sg!!/m&bo=wgEsAcIBLAERADc!&rf=mood_app",
          goods_id: 2,
          currentTab: 3
        },
        {
          src: "http://photogz.photo.store.qq.com/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzH5bSh7z60o5qB4Xa1HClKPb.JgKWjeSA.0XlVI6.aGr1pUwZezDcp6UW5lpPDEWEw!!/m&bo=MwGkADMBpAARADc!&rf=mood_app",
          goods_id: 3,
          currentTab: -1
        },
      ],
      indicatorDots: true, //是否显示面板指示点
      indicatorColor: "#fff", //指示点颜色
      indicatorActiveColor: "#C69C6D", //当前选中的指示点颜色
      autoplay: true, //是否自动切换
      current: 0, //当前所在滑块的 index
      interval: 5000, //自动切换时间间隔	
      duration: 500, //滑动动画时长
      circular: true, //是否采用衔接滑动
      vertical: false, //滑动方向是否为纵向
      previousMargin: "0px", //前边距，可用于露出前一项的一小部分，接受 px 和 rpx 值
      nextMargin: "0px", //后边距，可用于露出后一项的一小部分，接受 px 和 rpx 值
      displayMultipleItems: 1, //同时显示的滑块数量
      skipHiddenItemLayout: false, //是否跳过未显示的滑块布局，设为 true 可优化复杂情况下的滑动性能，但会丢失隐藏状态滑块的布局信息
      easingFunction: "default", //指定 swiper 切换缓动动画类型
    },
    // 分类数据
    classifyData: [{
        src: "http://a1.qpic.cn/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzCAksiECtuqjnG2yvFxcz9m.iWFCLJxqc9edBkfN3qoezCgNp*YdZFCSusEtFRlzmA!!/c&ek=1&kp=1&pt=0&bo=vAK8ArwCvAIRADc!&tl=1&tm=1654257600&dis_t=1654258513&dis_k=c4b220f5f9715aea4f5146f23be3c319&sce=0-12-12&rf=0-18",
        title: "蛋糕"
      },
      {
        src: "http://a1.qpic.cn/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzIzx42AdZDSzM2B0Ir2YDVyIbuOCNZ7bhk*QuAmathFJYuwYrTcY5M38cT4fpaLcUQ!!/m&ek=1&kp=1&pt=0&bo=.ADLAPgAywARADc!&tl=1&tm=1654257600&dis_t=1654258474&dis_k=0b37db79606360219247f729287ec9d7&sce=0-12-12&rf=0-18",
        title: "水果"
      },
      {
        src: "http://a1.qpic.cn/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzCKHLpahgmOyTEF.zZq8OWDMKaZvig2xaYB7DsdtOrHrupOvDvhoUJ6cbA5lT28F3g!!/m&ek=1&kp=1&pt=0&bo=4QDhAOEA4QARADc!&tl=1&tm=1654257600&dis_t=1654258544&dis_k=f88390771f76e1c1b16331ba69ce1e04&sce=0-12-12&rf=0-18",
        title: "饮料"
      },
      {
        src: "http://photogz.photo.store.qq.com/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/ruAMsa53pVQWN7FLK88i5hkZ4o4ytYgrk.PTh2xS3r9jYG9kSDDljc1qJBtmSxi.C1HJ3Q4nJYHtPRAvGBiaAlLK8ynMgIwYCxA6TSuMP4I!/a&bo=4QDhAAAAAAARECc!",
        title: "蔬菜"
      },
    ],
    // 商品展示卡片数据
    cardData: {
      classifyList: []
    }
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let paraArr = ["新品", "人气", "生日"];

    this.GetHomeCakeData({
      tag: paraArr[0]
    });
    this.GetHomeCakeData({
      tag: paraArr[1]
    });
    this.GetHomeCakeData({
      tag: paraArr[2]
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  // 获取首页蛋糕数据
  GetHomeCakeData: function(para) {
    let tempArr = null;
    API.GetHomeCakeData(para).then(res => {
      console.log("GetHomeCakeData res : ", res, ", para: ", para);
      tempArr = {
        classifyInfo: para.tag,
        shoppingData: res
      };
      this.data.cardData.classifyList.push(tempArr);
      this.setData({
        cardData: this.data.cardData
      });
      console.log()
    }).catch(req => {
      console.log("GetHomeCakeData req : ", req, ", para: ", para);
    });
  },

  // 加入购物车
  AddShoppingCart: function(para) {
    API.AddShoppingCart(para).then(res => {
      console.log("AddShoppingCart res : ", res, ", para: ", para);
      wx.showToast({
        title: '加入成功',
      })
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
      return {
        title: res.target.dataset.shareinfo.title,
        path: res.target.dataset.shareinfo.path,
        imageUrl: res.target.dataset.shareinfo.imageUrl
      }
    } else {
      return {
        title: '全新商品，等你来选购哦！',
        path: 'pages/home/home',
        imageUrl: "http://a1.qpic.cn/psc?/V52b6GaG2x7Pbh0BMUXD0dr0Kd0kP58b/05RlWl8gsTOH*Z17MtCBzCraKWBydYmqGDbBS8OkfOilJLUrWPa*h9ejhfluoI04*nLiQaEtwxSTJxC6ytmzog!!/c&ek=1&kp=1&pt=0&bo=vgAJAb4ACQERADc!&tl=1&tm=1654257600&dis_t=1654258857&dis_k=ebf546c211a1ed3cfa027d55d9e8aaa4&sce=0-12-12&rf=0-18"
      }
    }
  },
  // 点击更多的跳转事件
  onMore: function(e) {
    wx.switchTab({
      url: '/pages/classify/classify',
    })
  },

  // 点击首页轮播图下面的分类导航跳转商品详情
  onGOclassify: function(e) {
    switch (e.currentTarget.dataset.type) {
      case "蛋糕":
        app.globalData.currentTab = 0;
        wx.switchTab({
          url: '/pages/classify/classify',
        })
        break;
      case "水果":
        app.globalData.currentTab = 3;
        wx.switchTab({
          url: '/pages/classify/classify',
        })
        break;
      case "饮料":
        app.globalData.currentTab = 2;
        wx.switchTab({
          url: '/pages/classify/classify',
        })
        break;
      case "蔬菜":
        app.globalData.currentTab = 5;
        wx.switchTab({
          url: '/pages/classify/classify',
        })
        break;
      default:
        break;
    }
  },

  // 点击首页列表卡片的添加购物车图标将商品添加到购物车
  onCardAddCart: function(e) {
    let uuid = wx.getStorageSync('uuid');
    let username = wx.getStorageSync('username');
    if (!uuid) {
      wx.navigateTo({
        url: "/pages/login/login"
      });
    } else {
      this.AddShoppingCart({
        goods_id: e.currentTarget.dataset.goods_id,
        goods_type: e.currentTarget.dataset.goods_type,
        username: username,
        en_name: e.currentTarget.dataset.en_name,
        name: e.currentTarget.dataset.name,
        img: e.currentTarget.dataset.img,
        spec: e.currentTarget.dataset.spec,
        price: e.currentTarget.dataset.price,
        count: 1,
        cutlery_content: e.currentTarget.dataset.cutlery_content,
        birthday_card: ""
      });
     
    }
  },

  // 点击首页列表卡片跳转商品详情
  onGoCardDetail: function() {
    wx.navigateTo({
      url: '/pages/detail/detail',
    })
  },

  // 点击首页轮播图跳转商品详情
  onGoDetail: function(e) {
    if (e.currentTarget.dataset.current_tab !== -1) {
      app.globalData.currentTab = e.currentTarget.dataset.current_tab;
      wx.switchTab({
        url: '/pages/classify/classify',
      })
    } else if (e.currentTarget.dataset.goods_id !== -1) {
      wx.navigateTo({
        url: '/pages/detail/detail?goods_id=' + e.currentTarget.dataset.goods_id,
      })
    }
  },
})