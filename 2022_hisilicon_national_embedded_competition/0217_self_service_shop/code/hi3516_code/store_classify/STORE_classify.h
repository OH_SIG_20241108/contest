#ifndef STORE_CLASSIFY_H
#define STORE_CLASSIFY_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "hi_comm_video.h"

#if __cplusplus
extern "C" {
#endif

/* load STORE detect and classify model */
HI_S32 Yolo3STOREDetectLoad(uintptr_t* model);

/* unload STORE detect and classify model */
HI_S32 Yolo3STOREDetectUnload(uintptr_t model);

/* STORE detect and classify calculation */
HI_S32 Yolo3STOREDetectCal(uintptr_t model, VIDEO_FRAME_INFO_S *srcFrm, VIDEO_FRAME_INFO_S *dstFrm);

#ifdef __cplusplus
}
#endif
#endif