#ifndef __COMMON_UART_H__
#define __COMMON_UART_H__

#include "hi_types.h"

HI_S32 UartInit(HI_S32 UartDev);

HI_S32 UartSend(HI_S32 UartDev, const HI_U8 *pu8DataBuf, HI_U8 u8DataLen, HI_BOOL bVerbose);

#endif