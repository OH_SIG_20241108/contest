#ifndef __COMMON_VI_H__
#define __COMMON_VI_H__

#include "hi_types.h"
#include "hi_common.h"

HI_S32 VI_Init(VI_DEV ViDev, VI_PIPE ViPipe, VI_CHN ViChn);

HI_VOID VI_DeInit(VI_DEV ViDev, VI_PIPE ViPipe, VI_CHN ViChn);

#endif