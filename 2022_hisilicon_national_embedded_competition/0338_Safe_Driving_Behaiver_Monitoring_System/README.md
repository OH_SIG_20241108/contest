# 吾驾之宝-安全驾驶行为监测预警系统

## 简介

本产品主要面向出租车、客车、公交车、货车等各类营运车辆。多重预警系统，从心率血氧、酒精检测、压力传感器检测到分心检测多维度预警，提高驾驶安全性；实时监控车辆驾驶过程，解决大批量车队管理难问题。

## 主要特性

1. 多角度判断驾驶员状态****:**通过酒精传感器检测驾驶员是否饮酒驾驶；压力传感器反应驾驶员握住方向盘的力度；分心模型检测驾驶员异常行为，系统根据检测结果做出快速处理。
2. 状态信息实时反馈：**各路传感器的采集信息及识别结果通过腾讯云物联网平台及时回传到微信小程序数据展示平台，实现信息详细记录，并可远程查看。
3. 心率血氧实时检测：**以ESP32为主控芯片的心率血氧检测手环可以实时检测驾驶员心率血氧数据，并借助于ESP32芯片内置的wifi能力，上传到云平台。

## 主要创新

（1）**采用多传感器融合判断驾驶员状态****:**MQ3酒精传感器、压力传感器从驾驶员**状态层面**进行感知，心率血氧检测从驾驶员**生理层面**进行感知，分心检测从驾驶员**行为层面**进行判断。

（2）**采用****yolov2****目标检测模型**：输入图像直接得到检测结果，部署更简单，借助于NNIE的硬件加速功能，识别更流畅。

（3）**系统配有微信小程序平台：**设备采集的数据通过Pegasus WIFI IOT开发板上传到云平台，并在微信小程序端进行可视化展示。

## 设计框架

![image-20220713182849819](https://picture.nongchatea.xyz/images/2022/07/13/image-20220713182849819.png)

本系统的硬件部分由Taurus AI Camera 开发板、Pegasus WIFI IOT开发板以及自主设计的ESP32开发板组成，Taurus与Pegasus开发板采用“串口”的方式进行数据通信与传输,ESP32直接借助其自身的wifi通信能力进行数据传输。其中Taurus AI Camera 主要负责运行目标检测算法，对分心行为进行快速识别；Pegasus 端主要负责对模型推理结果的后续处理以及传感器数据采集上传；自主设计的ESP32开发板完成心率血氧数据的采集，并上传到云端。

![吾驾之宝-安全驾驶行为监测预警系统](https://picture.nongchatea.xyz/images/2022/07/14/f65bfbe7f69256c2b7c7e159cc44f6b3.jpg)





