/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef IOT_CONFIG_H
#define IOT_CONFIG_H

// <CONFIG THE LOG
/* if you need the iot log for the development , please enable it, else please comment it */
#define CONFIG_LINKLOG_ENABLE   1

// < CONFIG THE WIFI
/* Please modify the ssid and pwd for the own */
#define CONFIG_AP_SSID  "nova 7 5G" // WIFI SSID
#define CONFIG_AP_PWD   "fa4d802b9d44" // WIFI PWD
/* Tencent iot Cloud user ID , password */

#define CONFIG_USER_ID    "HOM8IRUD7AHi3861;12010126;682ed;1664640000"
#define CONFIG_USER_PWD   "a0d1814ddb3a01dd20dd36257e64d5ea9fb022ead66209166ab56e9f495e7ae4;hmacsha256"
#define CN_CLIENTID     "HOM8IRUD7AHi3861" // Tencent cloud ClientID format: Product ID + device name
#endif

/*
#define CONFIG_USER_ID    "12V6FHBXQIdevice-hi3861;12010126;10ac8;1664985600"
#define CONFIG_USER_PWD   "59636dbc59a869ced6f7ff8dc1a7bf9c49b4a264161faa477011b6ec99c9ee65;hmacsha256"
#define CN_CLIENTID     "12V6FHBXQIdevice-hi3861" // Tencent cloud ClientID format: Product ID + device name
#endif 


Client ID     12V6FHBXQIdevice-hi3861
MQTT Username     12V6FHBXQIdevice-hi3861;12010126;10ac8;1664985600
MQTT Password      59636dbc59a869ced6f7ff8dc1a7bf9c49b4a264161faa477011b6ec99c9ee65;hmacsha256
*/