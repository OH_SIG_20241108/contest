#ifndef PAGE1DIALOG_H
#define PAGE1DIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QDebug>
extern QFont font1,font2,font3,font4;
extern QString ip;
extern QString port;
namespace Ui {
class Page1Dialog;
}

class Page1Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Page1Dialog(QWidget *parent = 0);
    ~Page1Dialog();

private:
    Ui::Page1Dialog *ui;
};

#endif // PAGE1DIALOG_H
