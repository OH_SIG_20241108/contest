#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "page1.h"
#include "page2.h"
#include "page3.h"
#include "page4.h"

extern QFont font1,font2;//字体类型使用指针会报错
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //四个界面页
    Page1* page1;
    Page2* page2;
    Page3* page3;
    Page4* page4;
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
