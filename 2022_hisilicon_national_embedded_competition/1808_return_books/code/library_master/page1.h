#ifndef PAGE1_H
#define PAGE1_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include "librarydatabase.h"
#include <QDateTime>
#include "page2.h"
#include "page1dialog.h"
#include <QMovie>
extern QString ip;
extern QString port;
extern libraryDataBase* database;//数据库，全局变量
extern BookDatabase* bookdatabase;
extern QList<Student> students;
extern QList<QList<QString>> BookBoxListList;
extern QFont font1,font2,font3,font4;
extern QList<QString> BookBoxList1;
extern QList<QString> BookBoxList2;
extern QList<QString> BookBoxList3;
extern QList<QString> BookBoxList4;
namespace Ui {
class Page1;
}

class Page1 : public QWidget
{
    Q_OBJECT

public:
    //explicit Page1(QWidget *parent = 0);
    Page1(Page2 *p);
    ~Page1();
    QTcpSocket* socket;
    QByteArray tcpRevbuf;
private:
    Ui::Page1 *ui;
};

#endif // PAGE1_H
