# 共享智能充——基于视觉感知与网络通信的新能源电动汽车智能共享充电桩 Pegasus code
***
作者： 黄璟烨 杨博帆 张志灿    
时间： 2022/6
***

## 1.demo_entry_cmsis.c
创建新的进程入口

## 2.wifi_connecter.c
wifi连接相关的函数

## 3.hal_iot_gpio_ex.c
控制相关的gpio导通与关断函数

## 4.oled_ssd1306.c/oled_demo.c
控制oled显示屏显示代码

## 5.led_example.c
调用gpio以及led亮灭的代码