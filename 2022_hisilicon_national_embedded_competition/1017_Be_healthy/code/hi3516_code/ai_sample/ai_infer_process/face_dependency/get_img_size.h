#ifndef GET_IMG_SIZE_H
#define GET_IMG_SIZE_H

#if __cplusplus
extern "C" {
#endif

void GetPicWidthHeight(const char* path, unsigned int *punWidth, unsigned int *punHeight);

#ifdef __cplusplus
}
#endif
#endif
