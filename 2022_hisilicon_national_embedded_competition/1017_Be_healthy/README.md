## 设计目的
如今内存就是金钱，所以对于一些不必要的视频录制往往耗费了大量的内存，大大提高了用户成本，而且人们也总是遗憾于对一些美好瞬间（如运动场上运动员的精彩瞬间等）没有及时地记录下来，所以我们针对这些问题我们团队拟结合 Taurus AI套件与Pegasus制套件设计出“基于视觉捕捉的摄影精灵”的作品。
## 应用领域
在这个万物互联的时代任何，人工智能已经渗透到了各个行业的深处，但是我们发现在摄影领域的应用较为稀少，同时手动的拍摄具有传统的通病，很可能错过一些精彩的瞬间。
我们的产品正是从行业的这一个痛点出发，设计出了一款基于深度学习的AI摄影精灵，从理论层面，他几乎可以渗透到需要摄制的各个行业，例如运动比赛跟随拍摄，个人训练跟随拍摄，甚至是个人训练的动作分析。我们团队现已完成了跳高运动比赛的跟随拍摄功能，同时将精彩瞬间进行录制与保存，并发送到服务器，释放内部空间，解放人们的双手，让精彩不再错过。

## Hi3516人体识别代码位置：

./code/Hi3516DV300/scenario/human_detect

## Hi3861外设控制与小程序通信代码位置：

./code/Hi3861/app_demo_uart.c