***\*一种基于微表情识别的独居老人智慧监护与陪伴系统\****



 

# ***\*第一部分\****  ***\*设计概述\****

## **1.1** ***\*设计目的\****
目前，面向老人监测、监护系统的现有系统中，仅仅提供的是预防意外事故的发生，而无法针对性的改善独居老人的精神状态和心理状态。鉴于此，为解决当前独居老人的问题，提出的一种基于微表情识别的独居老人智慧监护与陪伴系统，其设计微表情信息组件，快速识别独居老人的心理状态，同时配合可以模拟儿女语音的AI语音组件，以及环境传感器，即实现了安全监护效果，又提供了语音陪伴式聊天模式，极大程序的改善独居老人的心理状态与生活质量。同时，本系统设计，可在后期提供更多的拓展单元，将智能心率、血压仪等测量设备的健康数据导入，通过算法推测老人患病的可能性，提前实现预警，获得预先性的治疗效果。


## **1.2** ***\*应用领域\****

目前，中国老年化程度的加深，家庭结构小型化形成“421”的家庭结构，传统家庭养老功能减弱，独居老人群体数目将会不断增加，独居老人的护理、照料的需求就会不断提升。本设计的应用领域是属于一种智慧养老的范畴，即运用计算机视觉与人工智能算法，既要实现独居老人的安全监护，又要实现独居老人的智慧陪伴。日后，在一些养老院或者独居老人的室内，均可采用本设计，合理的满足老人的安全监护与智慧陪伴。

 

## **1.3** ***\*主要技术特点\****

技术1：人脸微表情采集组件。本组件是借助海思HisSpark开发套件的摄像模组以及OV5116摄像模组共同组成。其中人脸微表情算法的设计是使用MobileNet V2的优化版为模型框架，在本地服务器训练完毕后生成pytorch模型继而转换成caffe模型，满足Hi3516DV300的使用。
技术2：串口通讯与WiFi通讯。串口通讯与WiFi通讯均是借助开源的Demo实现。WiFi通讯需要与本系统设计的OV5116摄像模组以及温湿度传感器、OSA-4光照度传感器、气体传感器、人体感应模块连接。
技术3：智能语音识别芯片。本设计体现的关于独居老人的陪伴功能，根据老人的语音指令，完成一系列的动作。同时其语音反馈模块，是借助AI算法预先训练好的模型，使其能够模拟儿女的声音，让独居老人亲身的感受到子女在身边陪伴的感觉，大大减少一些独居老人的心理问题。
技术4：独居老人微表情自建库。依据人脸图像库的规范，构建老人表情数据集，同时了消除背景干扰因素、人脸尺寸位置不一等问题，对图像进行了人脸对齐、尺寸归一化和 gamma 校正等预处理模式，保证数据集的规范性与预测的准确性。

## **1.4** ***\*关键性能指标\****

性能指标1：人脸微表情识别的算法精准效果。本设计采用的是MobileNet V2结合SVM，在微表情识别中的效果高达83.2%，可以在一定程度上识别出独居老人的异常表情，如痛苦、悲伤等。
性能指标2：智能语音识别算法精准效果。本设计是采用智能语音识别芯片内嵌入语音算法，在实际中的效果高达80.2%，可谓尚佳。

## **1.5** ***\*主要创新点\****

（1）以预防事故发生为监护目的：基于人脸微表情识别老人的异常表情数据实现痛苦表情识别，其训练集使用大量老人表情样本。监测系统主动发现独居老人处于关节炎发作等身体有疼痛的状态，并向老人的护理人员发送疼痛表情图像及发出预警，通知护理人员及时上门探访，防止因疼痛而导致老人摔倒跌落，从而避免该因素引发的骨折等严重后果； 
（2）实现一个护工对多名独居老人的高效监控：系统综合独居老人环境监测数据及其精神状态数据，定期同时向护工监视端发送多名被监护老人的综合状态信息。老人也可主动采集心率、血压等健康数据，如发生异常，系统自动向护工发出预警系统使用微信小程序向护工发送独居老人的异常状态，并且可随时随地主动查询独居老人的精神状态。
（3）智能语音陪伴的安全守护：系统借助智能语音芯片与内置的算法，可以模拟儿女的语音，与独居来人进行一些简单的语音交流，同时开放式设置一些学习功能，智能系统在与老人交流中可以不断的学习，使其更适应老人的生存环境。此外，语音功能也可以实现一些简单的功能，比如，开关灯、开关窗帘等。

# ***\*第二部分\****  ***\*系统组成及功能说明\****

2.1整体介绍

本设计初步要完成两个角度的问题，一是，实现独居老人的安全监护问题，以各种传感器配合表情识别实现；二是， 为独居老人提供智慧性陪伴服务，以AI语音的方式实现。 为进一步表述一种基于微表情识别的独居老人智慧监护与陪伴系统的设计，现将独居老人监护系统的设计如下：

通过微表情摄像模组采集老人的表情画面，为了实现表情识别，本文设置特定的安装角度摄像头作为人脸表情采集设备，以便更有可能获得正面完整的面部图像，且该设备应在不干扰独居老人生活的前提下自动触发。为提高后续表情识别的识别率，在进行表情识别的算法前，针对采样设备所采集的图像，对这些角度拍摄的图像进行人脸检测，如果存在人脸区域则执行后续的表情识别算法。在摄像头采样后，需要利用人脸检测算法，判断是否采集到合格的人脸图像。提取人脸后，进一步提取人脸上的特定特征点，以确定双眼、鼻子、眉毛、嘴唇等在不同表情下的相对位置。因为摄像头每次拍照时，被拍照的对象脸部的方向和距离都有差异，在特征点提取后，应该对其进行归一化处理。海思HisSpark开发套件便有此功能。
 
本系统设计主要是为独居老人设计，故建立一个室内住房设计模型。其中1位置为窗帘；2、3、4、7位置上安装摄像模组，即人脸微表情采集组件；5的位置上安装环境监测组件；6的位置上安装智能安全监护与陪伴控制组件，其放置于客厅位置，可以方面的监护到独居老人在沙发上得状态。

# ***\*第三部分\****  ***\*完成情况及性能参数\****

Hi3861V100控制外围扩展硬件。Hi3516DV300实现分类网的AI应用(独居老人的异常表情分类)。表示的模块整体的设计框架，本设计主要以两个为例，即窗帘和灯具。光线传感器检测到室内的光线过强时，则会通过舵机1自动控制窗帘完成关闭操作；灯具的控制可由老人语音操作，在接收到老人的语音信号时，舵机2便可以完成开关灯的操作。图4是功能实现流程，即Hi3516DV300与智能安全监护系统在3516D侧的实现逻辑比较简单，首先通过sensor采集视频数据，采集完之后可以做一些裁剪、缩放的操作，随后将数据送给nnie模块做为老人异常表情分类，有具体的类别之后便向后端下发指令。VI模块将摄像组件采集的视频信息，经过vpss进行图像处理、裁剪缩放、帧率控制等，获取通道帧数据传输到nnie模块作出异常表情分类，然后由3861模块进行通信控制一系列的操作。



# ***\*第四部分\****  ***\*总结\****



## ***\*4.1\*******\*可扩展之处\****

设计构建的系统还可将智能心率、血压仪等测量设备的健康数据导入，推算独居老人患病的可能性，以及后续的常规体检时间。
此微表情信息采集模块，并不局限于一个，而是依据室内的房间需求设置多个。同时，信息采集组件是一种便携式模块化设计，可安置于数据传输范围之内的任何位置。AI语音陪伴系统须进一步优化，不能仅仅的局限于语音指令，需要更多的依赖于智能语音交互功能。希望这一点，在未来的研究设计中可以着重进行开发与设计，满足独居老人的安全陪伴性效果。
 

## ***\*4.2\*******\*心得体会\****

本设计一种基于微表情识别的独居老人智慧监护与陪伴系统，其设计微表情信息组件，快速识别独居老人的心理状态，同时配合可以模拟儿女语音的AI语音组件，以及环境传感器，即实现了安全监护效果，又提供了语音陪伴式聊天模式，极大程序的改善独居老人的心理状态与生活质量。同时，本实用新型的系统设计，可在后期提供更多的拓展单元，将智能心率、血压仪等测量设备的健康数据导入，通过算法推测老人患病的可能性，提前实现预警，获得预先性的治疗效果。
未来可在此次研究的基础上，在被监护端子系统设计可穿戴式的监护系统，在可穿戴设备上设置采样模块，采样可通过图传系统将采样的图像传至表情识别模块进行分析，同时可在可穿戴式设备设备上增设更多的健康数据的监测。希望本项设计可以尽早的完成，投入到实际的应用之中，毕竟人口老龄化问题严重，不仅会制约国家经济发展，也会增长年轻人的压力。


# **第五部分** ***\*参考文献\****




 

 

# ***\*第六部分\****  ***\*附录\****

附录1 模型的搭建部分
from torch import nn
import torch


def _make_divisible(ch, divisor=8, min_ch=None):
    """
    This function is taken from the original tf repo.
    It ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    """
    if min_ch is None:
        min_ch = divisor
    new_ch = max(min_ch, int(ch + divisor / 2) // divisor * divisor)# 自动向下取整
    # Make sure that round down does not go down by more than 10%.
    if new_ch < 0.9 * ch:
        new_ch += divisor
    return new_ch


class ConvBNReLU(nn.Sequential):# 卷积bn激活函数的组合层
    def __init__(self, in_channel, out_channel, kernel_size=3, stride=1, groups=1):
        padding = (kernel_size - 1) // 2# 权重参数根据kernel_size来设置的，如果是3的话padding为1 如果是1 的话就是0
        super(ConvBNReLU, self).__init__(
            nn.Conv2d(in_channel, out_channel, kernel_size, stride, padding, groups=groups, bias=False),
            nn.BatchNorm2d(out_channel),
            nn.ReLU6(inplace=True)
        )


class InvertedResidual(nn.Module):# 倒残差结构
    def __init__(self, in_channel, out_channel, stride, expand_ratio):# expand_ratio扩展因子
        super(InvertedResidual, self).__init__()
        hidden_channel = in_channel * expand_ratio# 隐层的channel
        self.use_shortcut = stride == 1 and in_channel == out_channel# 布尔变量 判断捷径分支是否被使用

        layers = []# 层列表
        if expand_ratio != 1:
            # 1x1 pointwise conv
            layers.append(ConvBNReLU(in_channel, hidden_channel, kernel_size=1))
        layers.extend([
            # 3x3 depthwise conv
            ConvBNReLU(hidden_channel, hidden_channel, stride=stride, groups=hidden_channel),
            # 1x1 pointwise conv(linear)
            nn.Conv2d(hidden_channel, out_channel, kernel_size=1, bias=False),
            nn.BatchNorm2d(out_channel),
        ])

        self.conv = nn.Sequential(*layers)

    def forward(self, x):
        if self.use_shortcut:# 是否使用捷径分支
            return x + self.conv(x)
        else:
            return self.conv(x)


class MobileNetV2(nn.Module):# 主网络
    def __init__(self, num_classes=1000, alpha=1.0, round_nearest=8):# 超参数alpha=1.0卷积核参数的倍倍率
        super(MobileNetV2, self).__init__()
        block = InvertedResidual
        input_channel = _make_divisible(32 * alpha, round_nearest)# 卷积核个数调整为round_nearest的整数倍 8的整数倍
        last_channel = _make_divisible(1280 * alpha, round_nearest)

        inverted_residual_setting = [
            # t, c, n, s
            [1, 16, 1, 1],
            [6, 24, 2, 2],
            [6, 32, 3, 2],
            [6, 64, 4, 2],
            [6, 96, 3, 1],
            [6, 160, 3, 2],
            [6, 320, 1, 1],
        ]

        features = []
        # conv1 layer
        features.append(ConvBNReLU(3, input_channel, stride=2))#第一层的结构
        # building inverted residual residual blockes
        for t, c, n, s in inverted_residual_setting:
            output_channel = _make_divisible(c * alpha, round_nearest)# 作出一个优化调整
            for i in range(n):
                stride = s if i == 0 else 1
                features.append(block(input_channel, output_channel, stride, expand_ratio=t))
                input_channel = output_channel
        # building last several layers
        features.append(ConvBNReLU(input_channel, last_channel, 1))
        # combine feature layers
        self.features = nn.Sequential(*features)

        # building classifier
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.classifier = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(last_channel, num_classes)
        )

        # weight initialization
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out')
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.zeros_(m.bias)

    def forward(self, x):
        x = self.features(x)
        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x
附录2 模型的训练部分
import os
import sys
import json

import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import transforms, datasets
from tqdm import tqdm

from model_v3 import mobilenet_v3_large


def main():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("using {} device.".format(device))

    batch_size = 4
    epochs = 30

    data_transform = {
        "train": transforms.Compose([transforms.RandomResizedCrop(224),
                                     transforms.RandomHorizontalFlip(),
                                     transforms.ToTensor(),
                                     transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])]),
        "val": transforms.Compose([transforms.Resize(256),
                                   transforms.CenterCrop(224),
                                   transforms.ToTensor(),
                                   transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])}

    data_root = os.path.abspath(os.path.join(os.getcwd(), "../.."))  # get data root path
    image_path = os.path.join(data_root, "data_set", "flower_data")  # flower data set path
    assert os.path.exists(image_path), "{} path does not exist.".format(image_path)
    train_dataset = datasets.ImageFolder(root=os.path.join(image_path, "train"),
                                         transform=data_transform["train"])
    train_num = len(train_dataset)

    # {'daisy':0, 'dandelion':1, 'roses':2, 'sunflower':3, 'tulips':4}
    flower_list = train_dataset.class_to_idx
    cla_dict = dict((val, key) for key, val in flower_list.items())
    # write dict into json file
    json_str = json.dumps(cla_dict, indent=4)
    with open('class_indices.json', 'w') as json_file:
        json_file.write(json_str)

    nw = min([os.cpu_count(), batch_size if batch_size > 1 else 0, 8])  # number of workers
    print('Using {} dataloader workers every process'.format(nw))

    train_loader = torch.utils.data.DataLoader(train_dataset,
                                               batch_size=batch_size, shuffle=True,
                                               num_workers=nw)

    validate_dataset = datasets.ImageFolder(root=os.path.join(image_path, "val"),
                                            transform=data_transform["val"])
    val_num = len(validate_dataset)
    validate_loader = torch.utils.data.DataLoader(validate_dataset,
                                                  batch_size=batch_size, shuffle=False,
                                                  num_workers=nw)

    print("using {} images for training, {} images for validation.".format(train_num,
                                                                           val_num))

    # create model
    net = mobilenet_v3_large(num_classes=5)

    # load pretrain weights
    # download url: https://download.pytorch.org/models/mobilenet_v2-b0353104.pth
    model_weight_path = "./mobilenet_v3_large.pth"
    assert os.path.exists(model_weight_path), "file {} dose not exist.".format(model_weight_path)
    pre_weights = torch.load(model_weight_path, map_location='cpu') # 载入预训练模型，是一个字典类型

    # delete classifier weights
    pre_dict = {k: v for k, v in pre_weights.items() if net.state_dict()[k].numel() == v.numel()}# 遍历权重字典
    missing_keys, unexpected_keys = net.load_state_dict(pre_dict, strict=False)# 将权重字典载入

    # freeze features weights
    for param in net.features.parameters():
        param.requires_grad = False

    net.to(device)

    # define loss function
    loss_function = nn.CrossEntropyLoss()

    # construct an optimizer
    params = [p for p in net.parameters() if p.requires_grad]
    optimizer = optim.Adam(params, lr=0.0001)

    best_acc = 0.0
    save_path = './MobileNetV3.pth'
    train_steps = len(train_loader)
    for epoch in range(epochs):
        # train
        net.train()
        running_loss = 0.0
        train_bar = tqdm(train_loader, file=sys.stdout)
        for step, data in enumerate(train_bar):
            images, labels = data
            optimizer.zero_grad()
            logits = net(images.to(device))
            loss = loss_function(logits, labels.to(device))
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()

            train_bar.desc = "train epoch[{}/{}] loss:{:.3f}".format(epoch + 1,
                                                                     epochs,
                                                                     loss)

        # validate
        net.eval()
        acc = 0.0  # accumulate accurate number / epoch
        with torch.no_grad():
            val_bar = tqdm(validate_loader, file=sys.stdout)
            for val_data in val_bar:
                val_images, val_labels = val_data
                outputs = net(val_images.to(device))
                # loss = loss_function(outputs, test_labels)
                predict_y = torch.max(outputs, dim=1)[1]
                acc += torch.eq(predict_y, val_labels.to(device)).sum().item()

                val_bar.desc = "valid epoch[{}/{}]".format(epoch + 1,
                                                           epochs)
        val_accurate = acc / val_num
        print('[epoch %d] train_loss: %.3f  val_accuracy: %.3f' %
              (epoch + 1, running_loss / train_steps, val_accurate))

        if val_accurate > best_acc:
            best_acc = val_accurate
            torch.save(net.state_dict(), save_path)

    print('Finished Training')


if __name__ == '__main__':
main()
附录3 模型的预测部分
import os
import json

import torch
from PIL import Image
from torchvision import transforms
import matplotlib.pyplot as plt

from model_v3 import mobilenet_v3_large


def main():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    data_transform = transforms.Compose(
        [transforms.Resize(256),
         transforms.CenterCrop(224),
         transforms.ToTensor(),
         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])

    # load image
    img_path = "2.png"
    assert os.path.exists(img_path), "file: '{}' dose not exist.".format(img_path)
    img = Image.open(img_path)
    plt.imshow(img)
    # [N, C, H, W]
    img = data_transform(img)
    # expand batch dimension
    img = torch.unsqueeze(img, dim=0)

    # read class_indict
    json_path = './class_indices.json'
    assert os.path.exists(json_path), "file: '{}' dose not exist.".format(json_path)

    with open(json_path, "r") as f:
        class_indict = json.load(f)

    # create model
    model = mobilenet_v3_large(num_classes=5).to(device)
    # load model weights
    model_weight_path = "./MobileNetV3.pth"
    model.load_state_dict(torch.load(model_weight_path, map_location=device))
    model.eval()
    with torch.no_grad():
        # predict class
        output = torch.squeeze(model(img.to(device))).cpu()
        predict = torch.softmax(output, dim=0)
        predict_cla = torch.argmax(predict).numpy()

    print_res = "class: {}   prob: {:.3}".format(class_indict[str(predict_cla)],
                                                 predict[predict_cla].numpy())
    plt.title(print_res)
    for i in range(len(predict)):
        print("class: {:10}   prob: {:.3}".format(class_indict[str(i)],
                                                  predict[i].numpy()))
    plt.show()


if __name__ == '__main__':
    main()