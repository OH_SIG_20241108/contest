wx.cloud.init({
  env: 'hi3861-mptt-test-8f7aboa6c125e4b'  //数据库ID
})
const db = wx.cloud.database({
  env: 'hi3861-mptt-test-8f7aboa6c125e4b'
})
Page({
  data: {
    userInfo: {},
    userN: '',
    passW: ''
  },
  userNameInput: function (e) {
    this.setData({
      userN: e.detail.value
    })
  },
  passWordInput: function (e) {
    this.setData({
      passW: e.detail.value
    })
  },
 loginBtnClick: function (a) {
    var that = this
    if (that.data.userN.length == 0 || that.data.passW.length == 0) {
      wx.showModal({
        title: '温馨提示：',
        content:'用户名或密码不能为空！',
        showCancel:false
      })
    } else {
      db.collection('ksyueying').where({username:that.data.userN}).get({
        success(res){
          if(that.data.userN != res.data[0].username){
            wx.showModal({
             title: '用户名错误',
              content: '用户名错误'//session中用户名和密码不为空触发
           });
         }
          else{
           if (that.data.passW == res.data[0].passward){
             wx.navigateTo({
               url: '/pages/type/type'
             })
           }
           else{
             wx.showModal({
               title: '密码错误',
               content: '密码错误'//session中用户名和密码不为空触发
             });
           }
          }
        }
 
      })
    }
  }
})