#include <stdio.h>
#include <stdlib.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "iot_gpio_ex.h"
#include "hi_io.h"
#include "iot_pwm.h"
#include "hi_pwm.h"

#include "app_pwm.h"

#define IOT_PWM_PORT_PWM0 0
#define IOT_PWM_PORT_PWM1 1

#define GPIOFUNC 0

int CarStartFlag = 0;
int AutoFlag = 0;

int LFreq = 97;
int RFreq = 99;

void gpio_control(unsigned int gpio, IotGpioValue value)
{
    IoTGpioInit(gpio);
    hi_io_set_func(gpio, GPIOFUNC);
    IoTGpioSetDir(gpio, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(gpio, value);
}

void car_gpioinit(void)
{
    //设置2路 PWM 输出

    // GPIO10:PWM1, GPIO11, GPIO12
    IoTGpioInit(HI_IO_NAME_GPIO_10);
    IoSetFunc(HI_IO_NAME_GPIO_10, IOT_IO_FUNC_GPIO_10_PWM1_OUT);
    IoTGpioSetDir(HI_IO_NAME_GPIO_10, IOT_GPIO_DIR_OUT);
    IoTPwmInit(IOT_PWM_PORT_PWM1);
    gpio_control(HI_IO_NAME_GPIO_11, IOT_GPIO_VALUE0);
    gpio_control(HI_IO_NAME_GPIO_12, IOT_GPIO_VALUE0);

    //GPIO7:PWM0, GPIO8, GPIO5
    IoTGpioInit(HI_IO_NAME_GPIO_7);
    IoSetFunc(HI_IO_NAME_GPIO_7, IOT_IO_FUNC_GPIO_7_PWM0_OUT);
    IoTGpioSetDir(HI_IO_NAME_GPIO_7, IOT_GPIO_DIR_OUT);
    IoTPwmInit(IOT_PWM_PORT_PWM0);
    gpio_control(HI_IO_NAME_GPIO_8, IOT_GPIO_VALUE0);
    gpio_control(HI_IO_NAME_GPIO_5, IOT_GPIO_VALUE0);
}

void car_backward(void)
{
    car_stop();
    TaskMsleep(1000);

    IoTPwmStart(IOT_PWM_PORT_PWM0, LFreq, 4000);
    gpio_control(HI_IO_NAME_GPIO_8, IOT_GPIO_VALUE0);
    gpio_control(HI_IO_NAME_GPIO_5, IOT_GPIO_VALUE1);
    

    IoTPwmStart(IOT_PWM_PORT_PWM1, RFreq, 4000);
    gpio_control(HI_IO_NAME_GPIO_11, IOT_GPIO_VALUE0);
    gpio_control(HI_IO_NAME_GPIO_12, IOT_GPIO_VALUE1);
}

void car_forward(void)
{
    car_stop();
    TaskMsleep(1000);

    IoTPwmStart(IOT_PWM_PORT_PWM0, LFreq, 4000);
    gpio_control(HI_IO_NAME_GPIO_8, IOT_GPIO_VALUE1);
    gpio_control(HI_IO_NAME_GPIO_5, IOT_GPIO_VALUE0);

    IoTPwmStart(IOT_PWM_PORT_PWM1, RFreq, 4000);
    gpio_control(HI_IO_NAME_GPIO_11, IOT_GPIO_VALUE1);
    gpio_control(HI_IO_NAME_GPIO_12, IOT_GPIO_VALUE0);
}

void car_left(void) 
{
    car_stop();
    TaskMsleep(1000);

    IoTPwmStop(IOT_PWM_PORT_PWM0);
    gpio_control(HI_IO_NAME_GPIO_8, IOT_GPIO_VALUE0);
    gpio_control(HI_IO_NAME_GPIO_5, IOT_GPIO_VALUE0);

    IoTPwmStart(IOT_PWM_PORT_PWM1, RFreq, 4000);
    gpio_control(HI_IO_NAME_GPIO_11, IOT_GPIO_VALUE1);
    gpio_control(HI_IO_NAME_GPIO_12, IOT_GPIO_VALUE0);
}

void car_right(void)
{
    car_stop();
    TaskMsleep(1000);
    
    IoTPwmStart(IOT_PWM_PORT_PWM0, LFreq, 4000);
    gpio_control(HI_IO_NAME_GPIO_8, IOT_GPIO_VALUE1);
    gpio_control(HI_IO_NAME_GPIO_5, IOT_GPIO_VALUE0);

    IoTPwmStop(IOT_PWM_PORT_PWM1);
    gpio_control(HI_IO_NAME_GPIO_11, IOT_GPIO_VALUE0);
    gpio_control(HI_IO_NAME_GPIO_12, IOT_GPIO_VALUE0);
}

void car_stop(void)
{
    IoTPwmStop(IOT_PWM_PORT_PWM0);
    gpio_control(HI_IO_NAME_GPIO_8, IOT_GPIO_VALUE0);
    gpio_control(HI_IO_NAME_GPIO_5, IOT_GPIO_VALUE0);

    IoTPwmStop(IOT_PWM_PORT_PWM1);
    gpio_control(HI_IO_NAME_GPIO_11, IOT_GPIO_VALUE0);
    gpio_control(HI_IO_NAME_GPIO_12, IOT_GPIO_VALUE0);
}

void mode_pwmFreq(int location, int Freq)
{
    if(location == 0)
        IoTPwmStart(IOT_PWM_PORT_PWM0, Freq, 4000);
    else
        IoTPwmStart(IOT_PWM_PORT_PWM1, Freq, 4000);
}


static void *PWMDemoTask(const char *arg)
{
    car_gpioinit();

    printf("PWMCntrolDemo start!\n");

    while(1) {
        //car_forward();
        TaskMsleep(3000);

        // car_backward();
        // TaskMsleep(3000);

        // car_left();
        // TaskMsleep(3000);

        // car_right();
        // TaskMsleep(3000);
    }

    return NULL;
}

static void PWMDemo(void)
{
    car_gpioinit();

    osThreadAttr_t attr;
    attr.name = "PWMDemoTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /* 任务大小4096 */
    attr.priority = osPriorityNormal;
    if (osThreadNew(PWMDemoTask, NULL, &attr) == NULL) {
        printf("[PWMDemo] Failed to create PWMDemoTask!\n");
    }
}
SYS_RUN(PWMDemo);
