/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include "net_demo.h"
#include "net_params.h"
#include "wifi_connecter.h"
#include "robot_l9110s.h"


static void NetDemoTask(char * arg)
{      

    // char * t = arg;
    // *t = 3;
    // printf("cmd t = %d",*t);
    // printf("cmd arg = %d",*arg);


    int netId = ConnectToHotspot();

    int timeout = 4; /* timeout 10ms */
    while (timeout--) {
        printf("After %d seconds, I will start lwip test!\r\n", timeout);
        osDelay(100); /* 延时100ms */
    }

    NetDemoTest(PARAM_SERVER_PORT, PARAM_SERVER_ADDR, arg);

    printf("disconnect to AP ...\r\n");
    DisconnectWithHotspot(netId);
    printf("disconnect to AP done!\r\n");
}

static void mytest(char * arg){

      printf("mytest cmd = %d \r\n",*arg);
      car_direction_control_func(arg);
}

static void NetDemoEntry(void)
{

    char cmd = 0;
    char *mycmd = &cmd;
    char *mycmd2 = &cmd;

    osThreadAttr_t attr;

    attr.name = "NetDemoTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 10240; /* 堆栈大小为10240 */
    attr.priority = osPriorityNormal1;

    if (osThreadNew(NetDemoTask, mycmd, &attr) == NULL) {
        printf("[NetDemoEntry] Falied to create NetDemoTask!\n");
    }

    
    attr.name = "mytest";
    attr.stack_size = 1024*5; /* 堆栈大小为10240 */
    attr.priority = osPriorityNormal;
    if (osThreadNew(mytest, mycmd2, &attr) == NULL) {
        printf("[mytest] Falied to create NetDemoTask!\n");
    }
}

SYS_RUN(NetDemoEntry);