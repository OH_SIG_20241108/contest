/*
* 图像路径的更新在拍照页完成
* 具体查看拍照页saveImg()方法
*/

Page({

  /**
   * 页面的初始数据
   */
  data: {

    // 判断显示文字还是图片(预览图片标识)
		frontShow: true,
		// 身份证正面路径
		frontSrc: '',

		//判断显示文字还是图片(预览图片标识)
    otherShow: true,
    
		//身份证反面路径
    otherSrc: '',

  },

  // 拍摄身份证正面-跳转到拍摄页
  goScan: function() {
    wx.navigateTo({
			url: '/pages/scan/scan',
		})
  },
    
  instruction:function(){
      wx.navigateTo({
        url: '/pages/result_list/result_list',
      })
    
  },

  result:function(){
    wx.navigateTo({
      url: '/pages/index/index',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
	// //创建一个 TCP Socket 实例
	// const tcp = wx.createTCPSocket() 
	
	// export default {
	// 	data() {
	// 		return {							

	// 		}
	// 	},
		
	// 	onLoad() {
	// 		//启动连接
	// 		tcp.connect({
	// 			address: '192.168.1.87', 
	// 			port: 8866
	// 		})
			
	// 		//连接成功建立的时候触发该事件
	// 		tcp.onConnect(function(e){
	// 		    console.log('连接成功')				
	// 		})
			
	// 		//接收到数据的时候触发该事件
	// 		tcp.onMessage(function(e){
	// 			console.log(e.message)
	// 			//ArrayBuffer转16进制字符串
	// 			let buffer = e.message
	// 			let hexstr = Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('')
	// 			console.log(hexstr)
	// 		})	
	// 	},
		
	// 	onUnload() {
	// 		//关闭连接
	// 		tcp.close( )
	// 	},
		
		
	// 	methods: {			
	// 		btnTCP(){
	// 			//将一个十六进制报文转为字符数组
	// 			let str = 'cf 03 00 00 00 00 1e 59 ce 96 7f 01 00 00 a5'
	// 			let strs = str.split(" ") 				
	// 			for(let i = 0;i<strs.length;i++){				
	// 			　　strs[i] = "0x"+strs[i]	 //每个字符加上0x			
	// 			}				
	// 			let buffer = Buffer.from(strs) //将数组放到buffer				

	// 			//发送数据
	// 			tcp.write(buffer)			
	// 		}
	// 	}			
	// }
